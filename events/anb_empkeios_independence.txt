﻿namespace = empkeios

empkeios.1 = {
    type = country_event
    placement = ROOT

    title = empkeios.1.t
    desc = empkeios.1.d
    flavor = {
        triggered_desc = {
            trigger = { is_subject = yes }
            desc = empkeios.1.fs
        }
        triggered_desc = {
            trigger = { is_subject = no }
            desc = empkeios.1.ff
        }
    }

    event_image = {
		video = "ep1_transfer_of_authority"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_map.dds"

	duration = 3

    trigger = {
        c:C32 = ROOT
        NOT = {  
            ROOT = { is_subject_of = c:B05 } 
        }  
        NOT = { has_variable = empkeios_free }
    }

    immediate = {
        top_overlord = {
			save_scope_as = empkeios_benefactor
		}
        set_variable = empkeios_free
    }

    option = {
        name = empkeios.1.a
        default_option = yes
        custom_tooltip = empkeios.1.tt1
        remove_primary_culture = cu:vanburian
        add_primary_culture = cu:devabhen
        hidden_effect = {
            create_character = {
                culture = cu:devabhen
                ruler = yes
            }
        }
        custom_tooltip = empkeios.1.tt2
        if  = {
            limit = { is_subject = no }
            add_modifier = {
                name = empkeios_independence_modifier
                months = 60
            }
            add_loyalists = {
                culture = cu:devabhen
                value = 0.2
            }
        }
    }
}
