﻿object={
	name="twin_bridges"
	clamp_to_water_level=no
	render_under_water=no
	generated_content=no
	layer="cities"
	pdxmesh="Twin_Bridges_mesh"
	count=0
}
game_object_locator={
	name="twin_bridges_locator"
	clamp_to_water_level=no
	render_under_water=no
	generated_content=no
	layer="static"
	instances={
		{
			id=0
			position={ 1910.112671 0.000000 2463.238037 }
			rotation={ -0.000000 -0.000000 -0.000000 1.000000 }
			scale={ 5.670967 5.670967 5.670967 }
		}
	}
}
object={
	name="twin_bridges_variant_1"
	clamp_to_water_level=yes
	render_under_water=no
	generated_content=no
	layer="cities"
	pdxmesh="Twin_Bridges_mesh"
	count=2
	transform="1915.298218 -0.105468 2460.011475 0.000000 -0.965281 0.000000 0.261212 0.047999 0.047999 0.047999
1919.354126 -0.105468 2466.936279 0.000000 -0.965281 0.000000 0.261212 0.047999 0.047999 0.047999
"}
