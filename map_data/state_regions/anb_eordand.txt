﻿STATE_KARNEL = {
    id = 525
    subsistence_building = "building_subsistence_fishing_villages"
    provinces = { "x10C050" "x24D8BD" "x784FFA" "x904050" "xA316F5" "xC87A6E" "xF3D822" }
    traits = { state_trait_broken_sea_whaling state_trait_northern_aelantir_glaciers state_trait_magical_land_lesser }
    city = "xc87a6e"
    port = "x904050"
    farm = "xa316f5"
    wood = "x10C050"
    arable_land = 8
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 5
        bg_logging = 4
        bg_whaling = 5
    }
    naval_exit_id = 3115
}
STATE_FOGRACLEAK = {
    id = 605
    subsistence_building = "building_subsistence_farms"
    provinces = { "x03751A" "x0398EC" "x079650" "x0EED68" "x0FF6D2" "x13709A" "x17ECC0" "x364EC3" "x4BBE40" "x4CFEA7" "x5D444F" "x6C7E85" "x76CB42" "x7DEFA7" "x820D48" "x9C3DDD" "x9CC9DD" "x9D055C" "x9D7FA3" "xB7FBF7" "xC5AF45" "xD04149" "xE04275" "xE762B6" "xEA7915" "xED600A" "xF191ED" "xFFC0E3" }
    traits = { state_trait_harafe_desert state_trait_magical_land_lesser }
    city = "x9C3DDD"
    port = "x4BBE40"
    farm = "xED600A"
    wood = "xE762B6"
    arable_land = 46
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 10
        bg_logging = 2
        bg_coal_mining = 48
        bg_sulfur_mining = 42
		bg_relics_digsite = 5
    }
    naval_exit_id = 3122
}
STATE_TRIMTHIR = {
    id = 606
    subsistence_building = "building_subsistence_farms"
    provinces = { "x077129" "x0C9C9B" "x0CEFB2" "x0D1BD9" "x39D50C" "x4253B0" "x484273" "x537EC8" "x5C8451" "x66BB42" "x7A6B72" "x7AEA20" "x9CC997" "x9CD9BA" "x9EF337" "xBEDB7A" "xCF447E" "xD5FD1F" "xDB2055" "xDD6BA7" "xFA3511" }
    traits = { state_trait_harafe_desert state_trait_magical_land_lesser }
    city = "x7AEA20"
    port = "x7AEA20"
    farm = "x9CC997"
    wood = "x9EF337"
    arable_land = 32
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 4
        bg_coal_mining = 43
        bg_lead_mining = 32
    }
    naval_exit_id = 3122
}
STATE_MURDKATHER = {
    id = 607
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0815F8" "x30FF54" "x357C56" "x3CDCC1" "x49514D" "x4D3CFE" "x537B45" "x5D59EF" "x68485D" "x6FF5B3" "x879D87" "x8AF2D1" "x957E24" "xB0BDDF" "xB510A0" "xBBC64B" "xBDDF42" "xC35581" "xC57AF8" "xC6CBAA" "xCB84AE" "xCD7775" "xD59DA5" "xD7F1A0" "xE18E42" "xE1F4B6" "xE779CB" }
    traits = { state_trait_natural_harbors state_trait_magical_land_lesser }
    city = "xCD7775"
    port = "xD7F1A0"
    farm = "xD59DA5"
    wood = "xB510A0"
    arable_land = 120
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_silk_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 12
        bg_logging = 8
        bg_lead_mining = 43
		bg_whaling = 5
    }
    naval_exit_id = 3122
}
STATE_DEARKTIR = {
    id = 608
    subsistence_building = "building_subsistence_farms"
    provinces = { "x00E145" "x03AB0E" "x042425" "x08352F" "x14AA83" "x1F3CBC" "x31A515" "x40BD03" "x4445D8" "x54ED57" "x651C78" "x74B09D" "x8CD931" "xB00358" "xB05D8E" "xC159EB" "xD5F2A5" "xDC071B" "xF65C3E" "xF8BE0A" "xFC61BE" "xFF135A" "xFF15A0" }
    traits = { state_trait_magical_land_lesser }
    city = "xFF135A"
    port = "xFF15A0"
    farm = "x40BD03"
    wood = "xD5F2A5"
    arable_land = 60
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 12
        bg_iron_mining = 12
    }
    naval_exit_id = 3122
}
STATE_PASKALA = {
    id = 609
    subsistence_building = "building_subsistence_farms"
    provinces = { "x190D46" "x252EE2" "x28040A" "x370E78" "x47AD42" "x519467" "x51CD31" "x91395E" "x9B0578" "xDA8733" "xE6FC73" }
    traits = { state_trait_magical_land_lesser }
    city = "x9B0578"
    port = "x252EE2"
    farm = "x370E78"
    wood = "x519467"
    arable_land = 62
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 10
        bg_logging = 8
		bg_relics_digsite = 5
    }
    naval_exit_id = 3122
}
STATE_GATHGOB = {
    id = 610
    subsistence_building = "building_subsistence_farms"
    provinces = { "x10869B" "x2FF5A5" "x3020D0" "x370150" "x370A28" "x5E9DB3" "x63F8B6" "x690B78" "x9B0CA0" "xD49DAA" }
    traits = { state_trait_magical_land_lesser }
    city = "x370A28"
    port = "x370A28"
    farm = "x10869B"
    wood = "x5E9DB3"
    arable_land = 68
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 6
		bg_relics_digsite = 5
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
    naval_exit_id = 3122
}
STATE_PELODARD = {
    id = 611
    subsistence_building = "building_subsistence_farms"
    provinces = { "x10F59B" "x31E8CA" "x382790" "x4912A9" "x76556B" "x889891" "x89C710" "xBCA445" "xC2942F" "xCABCB7" "xD723DF" "xF30890" "xFF1478" "xFF7800" }
    traits = { state_trait_magical_land_lesser }
    city = "x4912A9"
    farm = "x10F59B"
    wood = "xFF7800"
    arable_land = 57
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 14
    }
}
STATE_DARHAN = {
    id = 612
    subsistence_building = "building_subsistence_farms"
    provinces = { "x67E296" "x7D04EC" "x8C20F8" "x907C77" "x9140E8" "x9680EC" "xF80319" "xFF1028" "xFF1178" }
    traits = { state_trait_magical_land_lesser }
    city = "x8C20F8"
    port = "x9140E8"
    farm = "xFF1178"
    wood = "xFF1028"
    arable_land = 79
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_silk_plantations }
    capped_resources = {
        bg_fishing = 7
        bg_logging = 12
        bg_lead_mining = 39
        bg_damestear_mining = 3
		bg_relics_digsite = 5
    }
    naval_exit_id = 3123
}
STATE_TRASAND = {
    id = 613
    subsistence_building = "building_subsistence_farms"
    provinces = { "x115D6B" "x3851B8" "x4B0428" "x7A3EAB" "x9CAC2D" "xA83B15" "xB5E805" "xD38C84" "xD40677" "xE612A0" "xFB1124" "xFF0FA0" }
    traits = { state_trait_magical_land_lesser }
    city = "xFF0FA0"
    farm = "x4B0428"
    wood = "xE612A0"
    arable_land = 62
    arable_resources = { bg_maize_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 12
        bg_lead_mining = 46
        bg_coal_mining = 19
		bg_relics_digsite = 5
    }
}
STATE_EINNSAG = {
    id = 614
    subsistence_building = "building_subsistence_farms"
    provinces = { "x15D84E" "x38EAAF" "x402CA0" "x410728" "x45F8A6" "x6909A0" "x6A8BCC" "x7200F8" "x82B573" "x9B0878" "xA966AD" "xB84608" "xC77AE4" }
    traits = { state_trait_magical_land_lesser }
    city = "x402CA0"
    port = "xA966AD"
    farm = "x9B0878"
    wood = "x6909A0"
    arable_land = 60
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 10
        bg_whaling = 7
        bg_sulfur_mining = 63
        bg_coal_mining = 25
        bg_gem_mining = 13
    }
    naval_exit_id = 3123
}
STATE_SGLOLAD = {
    id = 615
    subsistence_building = "building_subsistence_farms"
    provinces = { "x18E295" "x1B194C" "x285820" "x49EDD7" "x5C2E6A" "x6E00E8" "x8208F0" "x8E6EC0" "xACC4E5" "xC73A03" "xF884B7" }
    traits = { state_trait_magical_land_lesser }
    city = "x8E6EC0"
    port = "x6E00E8"
    farm = "x285820"
    wood = "x8208F0"
    arable_land = 37
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_dye_plantations }
    capped_resources = {
        bg_fishing = 5
        bg_logging = 8
        bg_whaling = 5
        bg_lead_mining = 110
		bg_relics_digsite = 5
    }
    naval_exit_id = 3123
}
STATE_RAITHLOS = {
    id = 617
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1B163E" "x36A4B5" "x6903A0" "x6906A0" "x6B00F0" "x7802E8" "x9B0278" "xC67EAD" "xD4CE71" "xD767FD" "xD9E541" }
    traits = { state_trait_magical_land_lesser }
    city = "x6B00F0"
    port = "x6B00F0"
    farm = "x6906A0"
    wood = "x6903A0"
    arable_land = 53
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 4
        bg_logging = 10
        bg_lead_mining = 53
    }
    naval_exit_id = 3123
}
STATE_FOGHARBAC = {
    id = 618
    subsistence_building = "building_subsistence_farms"
    provinces = { "x03730C" "x037910" "x122FA6" "x14BC08" "x24C935" "x274C7E" "x46808B" "x5A21E2" "x66F000" "x6700F4" "x6900E8" "x6F8623" "x7222A5" "x7DEC8E" "x84BC04" "x8710F4" "x8B8538" "xA3C1FB" "xB30236" "xC41FF6" "xC6C3F4" "xC887FF" "xD87A4F" }
    traits = { state_trait_magical_land_lesser }
    city = "xD87A4F"
    port = "x66F000"
    farm = "x6900E8"
    wood = "x8710F4"
    arable_land = 42
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_dye_plantations }
    capped_resources = {
        bg_fishing = 5
        bg_logging = 12
        bg_lead_mining = 32
		bg_relics_digsite = 5
    }
    naval_exit_id = 3123
}
STATE_DARTIR = {
    id = 619
    subsistence_building = "building_subsistence_farms"
    provinces = { "x09D3DD" "x1B1BB1" "x241F84" "x296020" "x31C779" "x47645D" "x4BE041" "x587C7E" "x5E0208" "x5F00E8" "x6000EC" "x627354" "x64FB8C" "x6800F8" "x7C73E4" "x85C631" "x85E4B2" "x8B0397" "x91A3C7" "x9FF854" "xAA68DD" "xAD296B" "xAD3DCD" "xB01AB1" "xB0D75A" "xB40988" "xC12FA5" "xC6AEBB" "xD5A35B" "xD76EAD" "xD8724F" "xE548A6" "xE5505D" "xEB9B9F" }
    traits = { state_trait_magical_land_lesser }
    city = "xD76EAD"
    port = "x6800F8"
    farm = "x5F00E8"
    wood = "x6000EC"
    arable_land = 29
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 8
        bg_sulfur_mining = 49
		bg_relics_digsite = 5
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 8
    }
    naval_exit_id = 3115
}
STATE_GEMRADCURT = {
    id = 620
    subsistence_building = "building_subsistence_farms"
    provinces = { "x01D859" "x025F45" "x05274B" "x0987D5" "x1A46BB" "x2715F1" "x31525D" "x3E75C6" "x505634" "x527285" "x587820" "x598020" "x6400E8" "x6D639D" "x854FAD" "x8E8E7F" "x938B08" "xA746AD" "xAD043F" "xB8E582" "xC1880F" "xC786AD" "xC78A4F" "xD7A2D4" "xDD9ECC" "xE1EB97" "xE551FF" "xE8C487" "xED6882" }
    traits = { state_trait_randrunnse_fishing state_trait_magical_land_lesser }
    city = "xC786AD"
    port = "x587820"
    farm = "x598020"
    wood = "xC78A4F"
    arable_land = 41
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 5
        bg_logging = 16
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 12
    }
    naval_exit_id = 3115
}
STATE_JHORGASHIRR = {
    id = 621
    subsistence_building = "building_subsistence_farms"
    provinces = { "x075475" "x08E279" "x209E52" "x234C8A" "x25A6F0" "x2CD927" "x3A70D5" "x43FA2B" "x46F7D0" "x5100EC" "x587020" "x58747E" "x619C92" "x6200F4" "x6D2304" "x6F00EC" "x7A1C3D" "x8748DC" "x886D5A" "x8B8B89" "x90C40C" "x9E77A4" "xB0906D" "xBB49E7" "xBB65EB" "xBD5FD3" "xC5AAE8" "xC8613D" "xD876AD" "xE7C037" "xEB7F87" "xF4E911" "xF92CB2" }
    traits = { state_trait_randrunnse_fishing state_trait_magical_land_lesser }
    city = "x58747E"
    port = "xD876AD"
    farm = "x5100EC"
    wood = "x6F00EC"
    arable_land = 44
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 4
        bg_logging = 16
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 10
    }
    naval_exit_id = 3115
}
STATE_GALBHAN = {
    id = 622
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0FF8E2" "x177E8A" "x17A932" "x1D9AE0" "x37F05A" "x4350CA" "x489020" "x4C4C7E" "x4C547E" "x4F00F8" "x4F9AEC" "x5000E8" "x566004" "x6819FA" "x69A93D" "x92D14F" "x95E82E" "xA3E0F4" "xA9E736" "xC6EBC6" "xE2FE8D" "xEA29FC" "xEAA2B1" }
    traits = { state_trait_randrunnse_fishing state_trait_magical_land_lesser }
    city = "x4C4C7E"
    port = "x4F00F8"
    farm = "x5000E8"
    wood = "x489020"
    arable_land = 31
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 4
        bg_logging = 10
        bg_lead_mining = 90
        bg_sulfur_mining = 56
    }
    naval_exit_id = 3115
}
STATE_TRIMGARB = {
    id = 623
    subsistence_building = "building_subsistence_farms"
    provinces = { "x00C266" "x074872" "x1186AE" "x1648D1" "x17E69A" "x1B8A52" "x1DFBFD" "x27049B" "x293785" "x34AE32" "x36CEE9" "x3DE2E0" "x413AA0" "x43431E" "x46A9BC" "x55A085" "x5EC1E6" "x6950B2" "x6B2A8C" "x7793E7" "x81796B" "x863382" "x875648" "x930302" "x96FB39" "x97491A" "x97A007" "x9F14C6" "xA32D6B" "xAB39B2" "xB5100C" "xBE4DAA" "xC18317" "xC29F9A" "xCEF81C" "xD97EAD" "xE05F07" "xE378FA" "xE68AF0" "xF872B5" }
    traits = { state_trait_magical_land_lesser }
    city = "xC18317"
    farm = "x55A085"
    wood = "xD97EAD"
    arable_land = 80
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 6
        bg_lead_mining = 55
		bg_relics_digsite = 5
    }
    naval_exit_id = 3115
}
STATE_ARMONADH = {
    id = 624
    subsistence_building = "building_subsistence_farms"
    provinces = { "x00D02F" "x03A8D6" "x147CBD" "x254011" "x306624" "x341923" "x352C9C" "x42DD7E" "x478820" "x478C7E" "x6300F8" "x654DA5" "x6861B0" "x79D452" "x836A0F" "x93C5B5" "x945758" "x9D7A1A" "x9E6604" "xA7EBFB" "xB17755" "xB97EF8" "xC01B64" "xC85A75" "xC930F4" "xCDCCC8" "xE4F859" "xF7EC9C" "xF9114A" }
    traits = { state_trait_randrunnse_fishing state_trait_armonadh_iron state_trait_magical_land_lesser }
    city = "x478820"
    port = "x478820"
    farm = "x478C7E"
    wood = "x6300F8"
    arable_land = 26
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 5
        bg_logging = 12
        bg_iron_mining = 32
		bg_coal_mining = 22
    }
    naval_exit_id = 3115
}
STATE_ANHOLTIR = {
    id = 625
    subsistence_building = "building_subsistence_farms"
    provinces = { "x09D8DA" "x0E1D66" "x37367A" "x3AD800" "x425520" "x456A11" "x467C7E" "x468020" "x50C3D0" "x58F19C" "x6500EC" "x66F2C5" "x696A4A" "x6A00EC" "x71DDD1" "x7A853F" "x807116" "x87DFC4" "x8DF1FD" "x8DF5C6" "xB89B0F" "xB902C6" "xC1A870" "xC67A4F" "xE2E063" "xE973E1" "xF3C878" "xF93E74" "xFB1662" "xFBADE8" "xFC0956" }
    traits = { state_trait_armonadh_iron state_trait_magical_land_lesser }
    city = "x6500EC"
    port = "x6500EC"
    farm = "x468020"
    wood = "x425520"
    arable_land = 55
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 6
        bg_logging = 13
        bg_coal_mining = 41
        bg_iron_mining = 52
		bg_relics_digsite = 5
    }
    naval_exit_id = 3115
}
STATE_EGASACH = {
    id = 626
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x004B94" "x038E47" "x07374A" "x10F61C" "x16E659" "x1828C9" "x1941ED" "x1DB173" "x1EFB3A" "x22CDBA" "x2A1952" "x2BF3B9" "x311AD6" "x3B33B1" "x52EDA4" "x58F374" "x6790EC" "x688079" "x6C3C71" "x6E71AC" "x702AC9" "x78C0DB" "x78D19C" "x7EC8ED" "x822A9F" "x85D423" "x899C72" "x8A1727" "x8A7485" "x8DBFBF" "x90264E" "x927B23" "x92C564" "x96D770" "xA12CB0" "xA35A3D" "xA36BC1" "xAACC93" "xB26E55" "xB59026" "xB7944C" "xBA26C7" "xBE3137" "xC00424" "xC43822" "xC4C2EF" "xC58C09" "xC89A78" "xCB46AD" "xCC8119" "xCCFE0E" "xCD0B39" "xCF1454" "xD176DA" "xD6F1A0" "xDB2062" "xE4A8F5" "xE99B91" "xEAB174" "xEBDCB0" "xF0C9A0" "xF104E2" "xF6F1B8" "xFAA194" "xFAE186" "xFB0150" "xFC3DA8" "xFCA65B" "xFD0BE5" "xFFCFFB" }
    traits = { state_trait_harafe_desert state_trait_magical_land_lesser }
    city = "xCB46AD" #Random
    farm = "xCB46AD" #Random
    arable_land = 24
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_coal_mining = 28
    }
}
STATE_DOMANDROD = {
    id = 627
    subsistence_building = "building_subsistence_orchards"
    provinces = { "xD7C642" "xF85904" }
    traits = { state_trait_domandrod state_trait_magical_land }
    city = "xD7C642" #Random
    farm = "xD7C642" #Random
    wood = "xD7C642" #Random
    arable_land = 125
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_vineyard_plantations bg_cotton_plantations bg_silk_plantations bg_tea_plantations bg_tobacco_plantations bg_sugar_plantations bg_banana_plantations bg_dye_plantations bg_opium_plantations }
    capped_resources = {
        bg_logging = 10
        bg_gem_mining = 12
    }
	resource = {
        type = "bg_rubber"
        discovered_amount = 9
    }
}
STATE_SPRING_GLADE = {
    id = 628
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x20757C" "x518F5F" "x51CA5E" "x5D89AB" "x822A29" "x8C57CC" "x9751C7" "xA761BD" "xABC6D2" "xB81673" "xCFF6C0" "xD87091" "xDAAE24" "xFACE56" }
    traits = { state_trait_domandrod state_trait_magical_land }
    city = "x9751C7"
    farm = "x9751C7"
    wood = "x9751C7"
    arable_land = 100
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_vineyard_plantations bg_tobacco_plantations bg_banana_plantations bg_dye_plantations bg_opium_plantations }
    capped_resources = {
        bg_logging = 20
    }
}
STATE_SUMMER_GLADE = {
    id = 629
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x055BC1" "x1714AE" "x20E08D" "x4697B1" "x581E6D" "x5B51E3" "x8B4005" "x971B42" "xA1832C" "xA7EA86" "xA9E347" }
    traits = { state_trait_domandrod state_trait_magical_land }
    city = "x055BC1"
    farm = "x055BC1"
    wood = "x055BC1"
    arable_land = 150
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_vineyard_plantations bg_cotton_plantations bg_silk_plantations bg_tea_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 20
    }
}
STATE_WINTER_GLADE = {
    id = 630
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x0FFC13" "x21BC94" "x394653" "x61AE21" "x786C3E" "x8704E4" "xA4BEC7" "xAA91F9" "xD2C92F" "xDAD095" "xDC43C8" "xF2C8C5" }
    traits = { state_trait_domandrod state_trait_magical_land }
    city = "xD2C92F"
    farm = "xD2C92F"
    wood = "xD2C92F"
    arable_land = 50
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 60
        bg_coal_mining = 40
        bg_gem_mining = 6
    }
}
STATE_AUTUMN_GLADE = {
    id = 631
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x0A67A2" "x11CE09" "x143BE3" "x1CCF57" "x210777" "x34CCBF" "x3D548C" "x4B447E" "x544C0E" "x606B2F" "x64F79C" "x6AD3AE" "x6DF122" "x717C5D" "x8508E3" "x85EE5A" "x97E163" "xA24874" "xA29261" "xB28A6F" "xBA12FD" "xDBD252" "xDF823C" "xF11DBD" "xF1EF5B" "xF23AFF" "xF7D8B2" }
    traits = { state_trait_domandrod state_trait_magical_land }
    city = "x544C0E"
    farm = "x544C0E"
    wood = "x544C0E"
    arable_land = 50
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 30
        bg_iron_mining = 38
        bg_gem_mining = 4
    }
}
