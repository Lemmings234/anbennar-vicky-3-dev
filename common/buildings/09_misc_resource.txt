﻿building_logging_camp = {
	building_group = bg_logging
	icon = "gfx/interface/icons/building_icons/logging_camp.dds"
	city_type = wood
	required_construction = construction_cost_low
	terrain_manipulator = forestry
	levels_per_mesh = 5

	production_method_groups = {
		pmg_base_building_logging_camp
		pmg_hardwood
		pmg_anima_building_logging_camp #Anbennar
		pmg_equipment
		pmg_transportation_building_logging_camp
	}
	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_farming.dds"
}

building_rubber_plantation = {
	building_group = bg_rubber
	icon = "gfx/interface/icons/building_icons/rubber_lodge.dds"
	required_construction = construction_cost_low
	
	terrain_manipulator = forestry

	unlocking_technologies = {
		rubber_mastication
	}

	city_type = wood
	levels_per_mesh = 5
	
	production_method_groups = {
		pmg_base_building_rubber_plantation
		pmg_enhancements_rubber_plantation # Anbennar
		pmg_train_automation_building_rubber_plantation
	}
	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_plantations.dds"
}

building_fishing_wharf = {
	building_group = bg_fishing
	icon = "gfx/interface/icons/building_icons/fishing_wharf.dds"
	city_type = port
	required_construction = construction_cost_low

	production_method_groups = {
		pmg_base_building_fishing_wharf
		pmg_enhancements_fishing_wharf # Anbennar
		pmg_refrigeration_building_fishing_wharf
	}
	
	potential = {
		is_sea_adjacent = yes
	}
	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_fishing.dds"
}

building_whaling_station = {
	building_group = bg_whaling
	icon = "gfx/interface/icons/building_icons/whaling_station.dds"
	city_type = port
	required_construction = construction_cost_low
	
	unlocking_technologies = {
		navigation
	}

	production_method_groups = {
		pmg_base_building_whaling_station
		pmg_enhancements_whaling_station # Anbennar
		pmg_refrigeration_building_whaling_station
	}
	
	potential = {
		OR = { 
			is_sea_adjacent = yes
			has_state_trait = state_trait_kraken
		}
	}
	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_fishing.dds"
}


building_oil_rig = {
	building_group = bg_oil_extraction
	icon = "gfx/interface/icons/building_icons/oil_rig.dds"
	city_type = mine
	levels_per_mesh = 5
	required_construction = construction_cost_medium

	unlocking_technologies = {
		pumpjacks
	}
	
	production_method_groups = {
		pmg_base_building_oil_rig
		pmg_treatments_oil_rig # Anbennar
		pmg_transportation_building_oil_rig
		pmg_automation_oil_rig # Anbennar
	}
	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_oil.dds"
}
