﻿ruler_elissa_template = {
	first_name = "Elissa"
	last_name = "Oathkeeper"
	historical = yes
	ruler = yes
	birth_date = 1374.4.23
	interest_group = ig_landowners
	#ig_leader = yes
	ideology = ideology_racial_purist
	dna = dna_elissa_lithiel
	traits = {
		ambitious psychological_affliction cruel trait_immortal
	}
	female = yes
	on_created = {
		set_character_immortal = yes
	}
}
ruler_elthora_shadowweaver = {
	first_name = "Elthora"
	last_name = "Shadowweaver"
	historical = yes
	ruler = yes
	ig_leader = yes
	female = yes
	age = 25
	dna = dna_elthora_shadowweaver
	interest_group = ig_intelligentsia
	ideology = ideology_magocrat
	traits = {
		aesthete engineer meticulous literary
	}
}
ruler_aratha_thunderchild = {
	first_name = "Aratha"
	last_name = "Thunderchild"
	historical = yes
	ruler = yes
	ig_leader = yes
	female = yes
	age = 32
	dna = dna_aratha_thunderchild
	interest_group = ig_devout
	ideology = ideology_liberal_leader
	traits = {
		pious charismatic inspirational_orator
	}
}
ruler_dak = {
	first_name = dak
	last_name = chaingrasper
	historical = yes
	age = 412
	culture = cu:undergrowth_goblin
	interest_group = ig_armed_forces
	ruler = yes
	ig_leader = yes
	ideology = ideology_authoritarian
	dna = dna_dak
	traits = {
		trait_immortal
		ambitious
		arrogant
		experienced_offensive_planner
	}
	on_created = {
		add_character_role = general
		set_character_immortal = yes
	}
}

ruler_arturo_silna_lioncost = {
	first_name = Arturo
	last_name = silna_Lioncost
	historical = yes
	ruler = yes
	noble = yes
	commander = yes
	age = 25
	interest_group = ig_armed_forces
	ideology = ideology_moderate
	dna = dna_arturo_silna_lioncost
	traits = {
		inspirational_orator popular_commander charismatic expensive_tastes
	}
	culture = cu:busilari
}

ruler_huszien = {
	first_name = huszien
	last_name = foxgoddess
	historical = yes
	ruler = yes
	female = yes
	ig_leader = no
	birth_date = 100.1.1
	interest_group = ig_rural_folk
	ideology = ideology_feminist
	dna = dna_placeholder_huszien
	culture = cu:gangim
	religion = rel:mystic_accord
	traits = {
		trait_immortal
		celebrity_commander
		firebrand
		pious
	}
	on_created = {
		set_variable = huszien_var
		set_character_immortal = yes
	}
}

ruler_horutep = {
	first_name = horutep
	last_name = horutep_titles
	historical = yes
	ruler = yes
	birth_date = 1641.1.1
	interest_group = ig_devout
	ideology = ideology_slaver
	culture = cu:sorrowmane_gnoll
	religion = rel:elikhetist
	traits = {
		trait_immortal
		pious
		ambitious
		expensive_tastes
		pillager
	}
	on_created = {
		add_character_role = general
		set_character_immortal = yes
	}
}

ruler_nuzha_genie = {
	first_name = Nuzha
	last_name = szal_Fazil
	historical = yes
	ruler = yes
	female = yes
	birth_date = 10.1.1
	interest_group = ig_landowners
	ig_leader = yes
	ideology = ideology_magocrat
	culture = cu:bahari
	religion = rel:old_sun_cult
	traits = {
		trait_immortal
		reserved
		imposing
		mountain_commander
	}
	on_created = {
		add_character_role = general
		set_character_immortal = yes
	}
}