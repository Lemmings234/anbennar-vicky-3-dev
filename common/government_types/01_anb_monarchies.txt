﻿gov_gray_empire = {
	transfer_of_power = hereditary
	
	male_ruler = "RULER_TITLE_GRAY_EMPEROR"
	female_ruler = "RULER_TITLE_GRAY_EMPRESS"

	male_heir = "" # Empty on purpose
	female_heir = "" # Empty on purpose
	
	possible = {
		exists = c:A10
		c:A10 = ROOT
		has_law = law_type:law_monarchy
		country_has_voting_franchise = no
	}

	on_government_type_change = {
		change_to_hereditary = yes
	}
	on_post_government_type_change = {
		post_change_to_hereditary = yes
	}
}

gov_eternal_empire = {
	transfer_of_power = dictatorial
	
	male_ruler = "RULER_TITLE_EMPEROR"
	female_ruler = "RULER_TITLE_EMPERESS"

	male_heir = "" # Empty on purpose
	female_heir = "" # Empty on purpose
	
	possible = {
		c:L01 = ROOT
		has_law = law_type:law_monarchy
	}

	on_government_type_change = {
		change_to_dictatorial = yes
	}
	on_post_government_type_change = {
		post_change_to_dictatorial = yes
	}
}

gov_akalate = {
	transfer_of_power = hereditary
	
	male_ruler = "RULER_AKAL"
	female_ruler = "RULER_AKALA"

	male_heir = "" # Empty on purpose
	female_heir = "" # Empty on purpose
	
	possible = {
		has_law = law_type:law_monarchy
		country_has_voting_franchise = no
		country_tier = principality
		any_primary_culture = {
			has_discrimination_trait = bulwari_heritage
		}
	}

	on_government_type_change = {
		change_to_hereditary = yes
	}
	on_post_government_type_change = {
		post_change_to_hereditary = yes
	}
}

gov_ynnic_lordship = {
	transfer_of_power = hereditary
	
	male_ruler = "RULER_TITLE_LORD"
	female_ruler = "RULER_TITLE_LADY"

	male_heir = "" # Empty on purpose
	female_heir = "" # Empty on purpose
	
	possible = {
		has_law = law_type:law_monarchy
		country_has_voting_franchise = no
		country_tier = principality
		country_has_state_religion = rel:ynn_river_worship
	}

	on_government_type_change = {
		change_to_hereditary = yes
	}
	on_post_government_type_change = {
		post_change_to_hereditary = yes
	}
}

gov_queendom = {
	transfer_of_power = hereditary
	
	male_ruler = "RULER_TITLE_QUEEN"
	female_ruler = "RULER_TITLE_QUEEN"

	male_heir = "" # Empty on purpose
	female_heir = "" # Empty on purpose
	
	possible = {
		exists = c:F13
		c:F13 = ROOT
		has_law = law_type:law_monarchy
		has_variable = is_matriarchy_var
		NOT = { has_law = law_type:law_womens_suffrage }
	}

	on_government_type_change = {
		change_to_hereditary = yes
	}
	on_post_government_type_change = {
		post_change_to_hereditary = yes
	}
}
gov_matriarchy = {
	transfer_of_power = hereditary
	
	male_ruler = "RULER_MATRIARCH"
	female_ruler = "RULER_MATRIARCH"

	male_heir = "" # Empty on purpose
	female_heir = "" # Empty on purpose
	
	possible = {
		has_law = law_type:law_monarchy
		has_variable = is_matriarchy_var
		NOT = { has_law = law_type:law_womens_suffrage }
	}

	on_government_type_change = {
		change_to_hereditary = yes
	}
	on_post_government_type_change = {
		post_change_to_hereditary = yes
	}
}