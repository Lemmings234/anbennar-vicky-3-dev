﻿pm_no_magical_construction = {
	texture = "gfx/interface/icons/production_method_icons/no_explosives.dds"
}

pm_fabrication_spells_buildings = {
	texture = "gfx/interface/icons/production_method_icons/iron_frame_buildings.dds"
	
	unlocking_technologies = {
		urban_planning
	}
	
	unlocking_production_methods = {
		pm_iron_frame_buildings
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_iron_add = -25
			goods_input_magical_reagents_add = 28
		}

		level_scaled = {
			building_employment_mages_add = 250
			building_employment_laborers_add = -250
		}
	}
}

pm_mimic_precursor_steel_buildings = {
	texture = "gfx/interface/icons/production_method_icons/arc_welded_buildings.dds"

	unlocking_technologies = {
		mimic_precursor_steel
	}
	
	unlocking_production_methods = {
		pm_steel_frame_buildings
		pm_arc_welded_buildings
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_steel_add = -25
			goods_input_flawless_metal_add = 18
		} 

		level_scaled = {
			building_employment_engineers_add = 250
			building_employment_laborers_add = -250
		}
	}
}

pm_builder_automata = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_unrecognized
		law_sapience_mechanim_compromise
	}
	unlocking_technologies = {
		magical_wave_theory			
	}



	unlocking_production_methods = {
		pm_iron_frame_buildings
		pm_steel_frame_buildings
		pm_arc_welded_buildings
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 6	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_arithmaton_engineering = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_unrecognized
		law_sapience_mechanim_compromise
	}

	unlocking_technologies = {
		advanced_mechanim			
	}
	unlocking_production_methods = {
		pm_steel_frame_buildings
		pm_arc_welded_buildings
	}
    


	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 12
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1000
			building_employment_clerks_add = -500
		}
	}
}

pm_builder_automata_enforced = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_unrecognized_enforced
		law_sapience_mechanim_compromise_enforced
	}
	is_hidden_when_unavailable = yes
	unlocking_technologies = {
		magical_wave_theory			
	}

	unlocking_production_methods = {
		pm_iron_frame_buildings
		pm_steel_frame_buildings
		pm_arc_welded_buildings
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 7
		}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_arithmaton_engineering_enforced = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_unrecognized_enforced
		law_sapience_mechanim_compromise_enforced
	}
	is_hidden_when_unavailable = yes
	unlocking_technologies = {
		advanced_mechanim			
	}
	unlocking_production_methods = {
		pm_steel_frame_buildings
		pm_arc_welded_buildings
	}
    
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 13
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1000
			building_employment_clerks_add = -500
		}
	}
}