﻿## Government Administrations
pm_arcane_religious_bureaucrats = {
	texture = "gfx/interface/icons/production_method_icons/mages_ownership.dds"

	disallowing_laws = {
		law_total_separation
		law_state_atheism
		law_traditional_magic_banned
	}
	
	country_modifiers = {
		workforce_scaled = {
			country_magical_expertise_add = 2.5
		}
	}

	building_modifiers = {
		level_scaled = {
			building_employment_clergymen_add = 175
			building_employment_archmages_add = 75
			building_employment_bureaucrats_add = 250
		}
	}	
	ai_value = 4
}

pm_arithmaton_calculators_government = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}
	unlocking_technologies = {
		advanced_mechanim			
	}


	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 10	
		}

		level_scaled = {
			building_employment_clerks_add = -1750
			building_employment_machinists_add = -1000
			building_employment_bureaucrats_add = -1000
		}
	}
}

pm_arithmaton_calculators_government_enforced = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	unlocking_technologies = {
		advanced_mechanim			
	}


	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 11	
		}

		level_scaled = {
			building_employment_clerks_add = -1750
			building_employment_machinists_add = -1000
			building_employment_bureaucrats_add = -1000
		}
	}
}

## Universities
pm_no_magical_studies = {
	texture = "gfx/interface/icons/production_method_icons/no_automation.dds"
}

pm_relics_reverse_enginering = {
	texture = "gfx/interface/icons/production_method_icons/philosophy_dept.dds"
	
	unlocking_technologies = {
		dialectics
	}

	country_modifiers = {
		workforce_scaled = { 
			country_weekly_innovation_add = 2
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_curiosity_add = 5
		}
		level_scaled = {
			building_employment_laborers_add = 1000
			building_employment_clerks_add = 400
			building_employment_academics_add = 100
		}		
	}
}

pm_precursor_immersion_studies = {
	texture = "gfx/interface/icons/production_method_icons/analytical_philosophy_department.dds"
	
	unlocking_technologies = {
		analytical_philosophy
	}

	country_modifiers = {
		workforce_scaled = { 
			country_weekly_innovation_add = 3
			country_weekly_innovation_max_add = 0.01
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_curiosity_add = 10
		}
		level_scaled = {
			building_employment_laborers_add = 500
			building_employment_clerks_add = 500
			building_employment_academics_add = 500
		}		
	}

}

pm_arcane_religious_academia = {
	texture = "gfx/interface/icons/production_method_icons/mages_ownership.dds"

	disallowing_laws = {
		law_total_separation
		law_state_atheism
		law_traditional_magic_banned
	}
	
	country_modifiers = {
		workforce_scaled = {
			country_magical_expertise_add = 5
		}
	}

	building_modifiers = {
		level_scaled = {
			building_employment_mages_add = 300
			building_employment_clergymen_add = 700
			building_employment_academics_add = 1000
		}
	}	
	ai_value = 4
}
pm_arithmaton_calculators_universities = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}
	unlocking_technologies = {
		advanced_mechanim			
	}
	unlocking_production_methods = {
		pm_philosophy_department
		pm_analytical_philosophy_department
	}


	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 5
		}

		level_scaled = {
			building_employment_clerks_add = -1000
			building_employment_laborers_add = -1000
		}
	}
}

pm_arithmaton_calculators_universities_enforced = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	unlocking_technologies = {
		advanced_mechanim			
	}
	unlocking_production_methods = {
		pm_philosophy_department
		pm_analytical_philosophy_department
	}


	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 6
		}

		level_scaled = {
			building_employment_clerks_add = -1000
			building_employment_laborers_add = -1000
		}
	}
}
