﻿regent_court = {
	texture = "gfx/interface/icons/religion_icons/regentcourt.dds"
	traits = {
		cannorian_religion cannorian_religion_tolerance
	}
	color = { 0.71 0.71 0.71 }
}

corinite = {
	texture = "gfx/interface/icons/religion_icons/corinite.dds"
	traits = {
		cannorian_religion cannorian_religion_tolerance
	}
	color = { 0.9 0.25 0.22 }
}

ravelian = {
	texture = "gfx/interface/icons/religion_icons/ravelian.dds"
	traits = {
		cannorian_religion cannorian_religion_tolerance
	}
	color = { 0.27 0.27 0.27 }
}

old_sun_cult = {
	texture = "gfx/interface/icons/religion_icons/oldsuncult.dds"
	traits = {
		sun_cult
	}
	color = { 1 0.26 0.09 }
}

new_sun_cult = {
	texture = "gfx/interface/icons/religion_icons/newsuncult.dds"
	traits = {
		sun_cult
	}
	color = { 1 0.79 0.05 }
}

the_jadd = {
	texture = "gfx/interface/icons/religion_icons/jadd.dds"
	traits = {
		sun_cult
	}
	color = { 1 0.54 0.05 }
}

the_thought = {
	texture = "gfx/interface/icons/religion_icons/thethought.dds"
	traits = {
		faithless cannorian_religion_tolerance
	}
	color = { 0.87 0.5 0.83 }
}

godlost = {
	texture = "gfx/interface/icons/religion_icons/godlost.dds"
	traits = {
		faithless
		raheni_religion
	}
	color = { 0.47 0 0.03 }
}

kheionism = {
	texture = "gfx/interface/icons/religion_icons/kheionism.dds"
	traits = {
		faithless
	}
	color = { 0.39 0.39 0.52 }
}

ancestor_worship = {
	texture = "gfx/interface/icons/religion_icons/ancestorworship.dds"
	traits = {
		dwarven_religion cannorian_religion_tolerance
	}
	color = { 0.34 0.34 0.34 }
}

dwarven_pantheon = {
	texture = "gfx/interface/icons/religion_icons/dwarvenpantheon.dds"
	traits = {
		dwarven_religion
	}
	color = { 0.39 0.39 0.39 }
}

runefather_worship = {
	texture = "gfx/interface/icons/religion_icons/runefatherworship.dds"
	traits = {
		dwarven_religion
	}
	color = { 0.2 0.12 0.2 }
}

skaldhyrric_faith = {
	texture = "gfx/interface/icons/religion_icons/skaldhyrric_faith.dds"
	traits = {
		gerudian_religion
	}
	color = { 1 0.79 0.79 }
}

mountain_watchers = {
	texture = "gfx/interface/icons/religion_icons/mountain_watchers.dds"
	traits = {
		gerudian_religion #temp
	}
	color = { 1 0.89 0.89 }
}

great_dookan = {
	texture = "gfx/interface/icons/religion_icons/old_dookan.dds"
	traits = {
		orcish_religion
	}
	color = { 0.17 0.42 0.24 }
}

old_dookan = {
	texture = "gfx/interface/icons/religion_icons/old_dookan.dds"
	traits = {
		orcish_religion
	}
	color = { 0.05 0.22 0.09 }
}

bulgu_orazan = {
	texture = "gfx/interface/icons/religion_icons/bulgu_orazan.dds"
	traits = {
		orcish_religion soruinic_religion
	}
	color = { 0.03 0.59 0.07 }
}

kobold_dragon_cult = {
	texture = "gfx/interface/icons/religion_icons/kobold_dragon_cult.dds"
	traits = {
		dragon_cult
	}
	color = { 0.01 0.33 0.49 }
}

nimrithan = {
	texture = "gfx/interface/icons/religion_icons/nimrithan.dds"
	traits = {
		dragon_cult
	}
	color = { 0.01 0.33 0.49 }
}

drozma_tur = {
	texture = "gfx/interface/icons/religion_icons/drozma_tur.dds"
	traits = {
		dragon_cult rzentur_religion ynnic_religion
	}
	color = { 0.24 0.7 0.1 }
}

goblinic_shamanism = {
	texture = "gfx/interface/icons/religion_icons/goblinic_shamanism.dds"
	traits = {
		goblin_religion
	}
	color = { 0.5 0.3 0.3 }
}

pactseeker = {
	texture = "gfx/interface/icons/religion_icons/pactseeker.dds"
	traits = {
		goblin_religion fey
	}
	color = { 0.1 0.4 0.4 }
}

gods_of_the_taychend = {
	texture = "gfx/interface/icons/religion_icons/gods_of_the_taychend.dds"
	traits = {
		taychendi_religion
	}
	color = { 0.74 0.58 0.29 }
}

oren_nayiru = {
	texture = "gfx/interface/icons/religion_icons/oren_nayiru.dds"
	traits = {
		taychendi_religion sun_cult
	}
	color = { 0.77 0.38 0.03 }
}

ynn_river_worship = {
	texture = "gfx/interface/icons/religion_icons/ynn_river_worship.dds"
	traits = {
		ynnic_religion
	}
	color = { 0.55 0.67 0.83 }
}

rune_scribes = {
	texture = "gfx/interface/icons/religion_icons/rune_scribes.dds"
	traits = {
		ynnic_religion
	}
	color = { 0.27 0.26 0.56 }
}

fey_court = {
	texture = "gfx/interface/icons/religion_icons/fey_court.dds"
	traits = {
		fey
	}
	color = { 0 0.45 0.73 }
}

eordellon = {
	texture = "gfx/interface/icons/religion_icons/eordellon.dds"
	traits = {
		fey eordan_religion
	}
	color = { 0.99 0.91 0.78 }
}

united_season_court = {
	texture = "gfx/interface/icons/religion_icons/united_season_court.dds"
	traits = {
		fey eordan_religion
	}
	color = { 0.99 0.91 0.78 }
}

spring_court = {
	texture = "gfx/interface/icons/religion_icons/spring_court.dds"
	traits = {
		fey eordan_religion
	}
	color = { 0.72 0.89 0.31 }
}

summer_court = {
	texture = "gfx/interface/icons/religion_icons/summer_court.dds"
	traits = {
		fey eordan_religion
	}
	color = { 0.96 0.75 0.21 }
}

autumn_court = {
	texture = "gfx/interface/icons/religion_icons/autumn_court.dds"
	traits = {
		fey eordan_religion
	}
	color = { 0.98 0.47 0.17 }
}

winter_court = {
	texture = "gfx/interface/icons/religion_icons/winter_court.dds"
	traits = {
		fey eordan_religion
	}
	color = { 0.79 0.96 1 }
}

vechnogejzn = {
	texture = "gfx/interface/icons/religion_icons/vechnogejzn.dds"
	traits = {
		fey rzentur_religion
	}
	color = { 0.68 0.21 0.10 }
}

high_philosophy = {
	texture = "gfx/interface/icons/religion_icons/high_philosophy.dds"
	traits = {
		raheni_religion
	}
	color = { 0.29 0.04 0.38 }
}

righteous_path = {
	texture = "gfx/interface/icons/religion_icons/righteous_path.dds"
	traits = {
		halessi_religion
	}
	color = { 0.87 0.87 0.87 }
}

lefthand_path = {
	texture = "gfx/interface/icons/religion_icons/devouring_path.dds"
	traits = {
		halessi_religion
	}
	color = { 0.22 0.22 0.22 }
}

mystic_accord = {
	texture = "gfx/interface/icons/religion_icons/mystic_accord.dds"
	traits = {
		halessi_religion
	}
	color = { 0.49 0.97 1 }
}

feast_of_the_gods = {
	texture = "gfx/interface/icons/religion_icons/feast_of_the_gods.dds"
	traits = {
		giantkin_religion
	}
	color = { 0.47 0.5 0.35 }
}

irdaeos_worship = {
	texture = "gfx/interface/icons/religion_icons/irdaeos_worship.dds"
	traits = {
		centaur_religion
	}
	color = { 0.24 0.16 0.08 }
}

kalyin_worship = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		triunic_religion
	}
	color = { 0.39 0.50 0.78 }
}

goddess_follower = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		triunic_religion
	}
	color = { 0.63 0.63 0.63 }
}

yudunyovi = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		triunic_religion
	}
	color = { 0.96 0.67 0.05 }
}

moitsaliusko = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		triunic_religion
	}
	color = { 0.77 1 0.87 }
}

elven_forebears = {
	texture = "gfx/interface/icons/religion_icons/elven_forebears.dds"
	traits = {
		elven_religion cannorian_religion_tolerance
	}
	color = { 0.73 0.95 0.97 }
}

soise_vio = {
	texture = "gfx/interface/icons/religion_icons/elven_forebears.dds"
	traits = {
		elven_religion
	}
	color = { 0.78 0.67 0.71 }
}

the_song = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		harpy_religion effelai_religion
	}
	color = { 0 0.29 0.35 }
}

raeldaeriach = {
	texture = "gfx/interface/icons/religion_icons/raeldaeriach.dds"
	traits = {
		eordan_religion
	}
	color = { 0.12 0.25 0.48 }
}

dai_nadeilhil = {
	texture = "gfx/interface/icons/religion_icons/dai_nadeilhil.dds"
	traits = {
		harafic_religion
	}
	color = { 0.63 0.50 0.48 }
}

eotomolaque = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		harafic_religion
	}
	color = { 0.90 0.51 0.27 }
}

decuvie = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		harafic_religion eordan_religion
	}
	color = { 0.38 0.22 0.74 }
}

kalun_masks = {
	texture = "gfx/interface/icons/religion_icons/kalun_masks.dds"
	traits = {
		harafic_religion
	}
	color = { 0.82 0.78 0.23 }
}

ja_akaiin = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		harafic_religion
	}
	color = { 0.34 0.39 0.52 }
}

dobondotist = {
	texture = "gfx/interface/icons/religion_icons/dobondotist.dds"
	traits = {
		noruinic_religion eldritch_cult
	}
	color = { 0.68 0.34 0.27 }
}

weeping_mother = {
	texture = "gfx/interface/icons/religion_icons/weeping_mother.dds"
	traits = {
		noruinic_religion
	}
	color = { 1 0.47 0.50 }
}

death_masks = {
	texture = "gfx/interface/icons/religion_icons/death_masks.dds"
	traits = {
		noruinic_religion
	}
	color = { 0.26 0.26 0.26 }
}

kinbond = {
	texture = "gfx/interface/icons/religion_icons/kinbond.dds"
	traits = {
		psychic_hivemind
	}
	color = { 0.44 0.08 0.49 }
}

etchings_of_the_deep = {
	texture = "gfx/interface/icons/religion_icons/etchings_of_the_deep.dds"
	traits = {
		etchings_of_the_deep_religion eldritch_cult
	}
	color = { 0.12 0.38 0.32 }
}

leechfather = {
	texture = "gfx/interface/icons/religion_icons/leechfather.dds"
	traits = {
		leechfather_religion eldritch_cult
	}
	color = { 0.09 0 0.35 }
}

senanset = {
	texture = "gfx/interface/icons/religion_icons/senanset.dds"
	traits = {
		soruinic_religion
	}
	color = { 0.43 0.79 0.15 }
}

tswohvwohii = {
	texture = "gfx/interface/icons/religion_icons/tswohvwohii.dds"
	traits = {
		effelai_religion
	}
	color = { 0.12 0.34 0.10 }
}

chahinanito = {
	texture = "gfx/interface/icons/religion_icons/chahinanito.dds"
	traits = {
		effelai_religion
	}
	color = { 0.35 0.50 0.08 }
}

song_servants = {
	texture = "gfx/interface/icons/religion_icons/song_servants.dds"
	traits = {
		effelai_religion
	}
	color = { 0.01 0.2 0 }
}

rioqay_diu = {
	texture = "gfx/interface/icons/religion_icons/rioqay_diu.dds"
	traits = {
		bharbhen_religion
	}
	color = { 1 0.79 0.06 }
}

gotiriei = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		bharbhen_religion
	}
	color = { 0.57 0.57 0.47 }
}

mazhthramazh = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		bharbhen_religion
	}
	color = { 0.57 0.70 0.57 }
}

askaeorg = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		bharbhen_religion
	}
	color = { 0.65 0.22 0.6 }
}

evikeraat_cult = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		eldritch_cult
	}
	color = { 25 45 65 }
}

nakavy_avo = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		fahvanosy_religion
	}
	color = { 226 98 95 }
}

bangujonsi = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		halessi_religion
	}
	color = { 67 179 105 }
}

Nusan_Banbe = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		halessi_religion
	}
	color = { 185 50 110 }
}

Sudomeg_i = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		halessi_religion
	}
	color = { 50 125 185 }
}

saanorgegh = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		halessi_religion
	}
	color = { 95 100 120 }
}

levaajan = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		halessi_religion
	}
	color = { 116 166 179 }
}

umun_omi = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		halessi_religion
	}
	color = { 0 63 63 }
}

un_khudai = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		halessi_religion
	}
	color = { 80 213 185 }
}