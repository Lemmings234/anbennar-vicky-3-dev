﻿magical_expertise_loc = {
	type = country

	text = {
		trigger = {
			country_magical_expertise_balance >= 0
		}
		localization_key = MAGICAL_EXPERTISE_SURPLUS_HEADER 
	}
	text = {
		trigger = {
			country_magical_expertise_balance < 0
		}
		localization_key = MAGICAL_EXPERTISE_DEFICIT_HEADER
	}
}

magical_expertise_usage_breakdown = {
	type = country
	text = {
		trigger = {
			country_num_spells > 0
		}
		localization_key = MAGICAL_EXPERTISE_SPELL_LEVELS_USAGE_SPELLS
	}
	# stuff for edicts here later
}