colors	=	{
	#Anbennar Colours
	# rgb { 28 124 140 }
	adean_blue = { 15 117 188 }

	adzalan_purple = rgb { 48 0 113 }
	adzalan_gold = rgb { 255 233 0 }

	arakeprun_purple = rgb { 114 61 126 }
	arakeprun_gold = rgb { 255 212 42 }
	
	arbaran_green = rgb { 39 74 39 }

	aelnar_blue	=	rgb { 10 27 143 }

	anbennar_white	=	rgb { 211  220  240 }
	
	amadian_white	=	rgb { 242  243  255 }

	amiratsamo_purple	=	rgb { 180 92 214 }
	amiratsamo_blue	=	rgb { 30 107 189 }
	amiratsamo_yellow	=	rgb { 255 187 28 }
	amiratsamo_orange	=	rgb { 252 108 5 }

	ancardian_brown	=	rgb { 186 121 104 }

	ardimyan_orange	=	rgb { 255 159 12 }
	ardimyan_brown	=	rgb { 35 21 18 }

	arg_ordstun_black	=	rgb { 43 48 57 }

	arganjuzorn_teal	=	rgb { 0 100 84 }

	argezvale_red	=	rgb { 211 135 151 }
	argezvale_light_grey	=	rgb { 164 173 194 }
	argezvale_med_grey	=	rgb { 108 117 138 }
	argezvale_dark_grey	=	rgb { 77 85 104 }

	azjakuma_red	=	rgb { 201 31 55 }
	azjakuma_white	=	rgb { 251 219 199 }

	west_tipney_green = rgb {24 120 11}
	west_tipney_blue = rgb {29 79 164}

	elathael_blue = rgb {0 197 230}

	triarchy_black = rgb {61 61 61}
	triarchy_pink = rgb {255 113 166}
	triarchy_green = rgb {114 205 112}
	triarchy_blue = rgb {118 183 241}

	amacimst_pink = rgb { 206 17 212 }
	amacimst_purple = rgb { 147 12 151 }
	
	amldihr_blue = rgb { 53 66 109 }

	arannen_blue_light	=	rgb { 100 150 190 }
	
	azkare_orange	=	rgb { 173 53 0 }
	
	baihon_orange = rgb { 185 60 10 }
	baihon_gold = rgb { 255 171 24 }

	bloodgroves_pink = rgb { 246 226 226 }
	bloodgroves_red = rgb { 135 0 0 }
	bloodgroves_brown = rgb { 132 119 119 }

	buffalo_blue = rgb { 40 50 70 }

	burad_grey_light	=	rgb { 147 147 147 }
	burad_grey_dark	=	rgb { 88 92 99 }
	burad_blue	=	rgb { 134 217 255 }

	busilari_orange	=	rgb { 211 85 4 } #Busilar's color in EU4

	bosancovac_blue = rgb { 51 51 68 }

	bwa_dakinshi_gold = rgb { 255 202 33 }

	castanorian_white	=	rgb { 243 243 244 }
	castanorian_silver	=	rgb { 206 216 223 }

	coddorran_light_green	=	rgb { 176 167 134 }
	coddorran_dark_green	=	rgb { 115 109 74 }
	
	corinsfield_yellow	=	rgb { 255 188 43 }

	corvurian_red_dark	=	rgb { 118 17 19 }
	corvurian_red_light	=	rgb { 161 28 47 }

	cyranvar_green = rgb { 22 64 7 }

	daengun_green	=	rgb { 61 158 51 }
	daengun_teal	=	rgb { 27 103 139 }

	damerian_blue	=	rgb { 28 124 140 }
	damerian_blue_light	=	rgb { 39 160 163 }
	damerian_black	=	rgb { 33 33 33 }
	damerian_white	=	rgb { 239 241 242 }

	derannic_purple	=	rgb { 102 45 145 }
	derannic_pink	=	rgb { 218 28 92 }

	dolindhan_gold = rgb { 255 182 24 }

	dragon_command_red	=	rgb { 99 0 0 }

	drevkenuc_brown = rgb { 120 68 33 }
	drevkenuc_light_brown = rgb { 160 90 44 }

	dur_vazhatun_blue = rgb { 39 36 95 }

	eborthili_gold	=	rgb { 207 182 100 } #Eborthil's color in EU4
	eborthili_gold_light	=	rgb { 237 223 131 }
	
	eighard_grey	=	rgb { 82 84 85 }
	eighard_red	=	rgb { 64 16 16 }

	einnsag_blue	=	rgb { 22 105 163 }

	eldritch_blue = rgb { 0 66 114 }

	enteben_red	=	rgb { 120 40 40 }
	
	eordan_grey = rgb { 153 153 153 }
	eordan_gold = rgb { 255 204 0 }
	eordan_gold_dark = rgb { 212 175 55 }

	estalli_green	=	rgb { 158 207 111 }

	feiten_blue	=	rgb { 0 108 183 }

	garlas_blue	=	rgb { 30 49 107 }

	gawedi_blue	=	rgb { 48 51 108 }
	gawedi_yellow	=	rgb {209 173 103 }

	ghankedhen_orange	=	rgb { 255 78 0 }

	gnomish_pink	=	rgb { 238 105 159 }

	jadd_orange	=	rgb { 255 138 12 }

	ranger_brown	=	rgb { 128 63 50 }
	ranger_blue	=	rgb { 48 62 124 }

	haraagtseda_light_red	=	rgb { 137 67 67 }
	haraagtseda_red	=	rgb { 87 13 13 }

	haraf_brown	=	rgb { 161 132 113 }
	haraf_beige	=	rgb { 243 242 215 }
	haraf_blue	=	rgb { 63 105 177 }

	hasakely_yellow	=	rgb { 212 140 16 }

	havoral_blue = rgb { 71 103 155 }
	havoral_grey = rgb { 52 59 70 }

	hierarchy_grey = rgb { 102 102 102 }
	hierarchy_pink = rgb { 248 80 149 }
	hierarchy_maroon = rgb { 155 37 88 }

	ibevar_blue	=	rgb { 210 237 246 }

	iochand_blue	=	rgb { 58 50 153 }
	iochand_green	=	rgb { 176 255 153 }

	irrliam_yellow	=	rgb { 230 200 80 }

	jagab_faru_gold	=	rgb { 250 214 115 }
	jagab_faru_yellow	=	rgb { 247 238 183 }

	jahzkredu_dark_green	=	rgb { 49 63 0 }
	jahzkredu_light_green	=	rgb { 145 203 89 }

	kaino_brown	=	rgb { 86 47 42 }

	karshyr_blue	=	rgb { 7 3 154 }
	karshyr_light_blue	=	rgb { 108 170 255 }
	karshyr_gold	=	rgb { 222 222 35 }

	kheios_teal	=	rgb { 60 120 170 }

	khet_orange	=	rgb { 230 172 85 }

	khasa_green	=	rgb { 90 135 65 }

	khatalashya_dark_blue	=	rgb { 1 12 57 }
	khatalashya_light_blue	=	rgb { 0 248 255 }

	khugdihr_light_brown	=	rgb { 137 113 78 }
	khugdihr_dark_brown	=	rgb { 128 106 76 }

	krakdhumvror_dark_blue	=	rgb { 42 87 163 }
	krakdhumvror_medium_blue	=	rgb { 72 138 201 }
	krakdhumvror_light_blue	=	rgb { 130 194 235 }
	krakdhumvror_grey_blue	=	rgb { 80 116 178 }

	kuzaram_teal	=	rgb { 24 167 175 }
	kuzaram_dark_teal	=	rgb { 49 61 61 }
	kuzaram_yellow	=	rgb { 246 189 70 }

	lodhum_grey_dark	=	rgb { 29 32 37 }
	lodhum_grey_med	=	rgb { 48 52 61 }
	lodhum_grey_light	=	rgb { 71 74 84 }
	lodhum_red_dark	=	rgb { 69 18 34 }
	lodhum_red_med	=	rgb { 143 24 42 }
	lodhum_red_light	=	rgb { 218 35 73 }
	lodhum_pink	=	rgb { 228 108 171 }
	lodhum_gold	=	rgb { 244 157 30 }

	lorentish_red	=	rgb { 237 28 36 }
	lorentish_red_dark	=	rgb { 164 29 33 }
	lorentish_green	=	rgb { 75 131 61 }
	lorentish_green_dark	=	rgb { 55 111 41 }
	lorentish_lilac	=	rgb { 168 128 186 }
	lorentish_gold	=	rgb { 255 239 159 }

	luoyip_brown = rgb { 79 56 1 }
	luoyip_tan = rgb { 141 112 26 }
	luoyip_yellow = rgb { 254 194 3 }

	endralliander_azure = rgb {63 72 204 }

	murdkather_green = rgb { 67 132 103 }
	murdkather_grey = rgb { 201 201 201 }
	murdkather_blue = rgb { 45 50 64 }

	minarian_peach	=	rgb { 255 221 176 }

	nahna_red	=	rgb { 172 30 11 }
	nahna_yellow	=	rgb { 255 168 0 }

	nanru_nakar_orange = rgb { 198 59 29 }
	nanru_nakar_yellow = rgb { 250 206 0 }
	nanru_nakar_dark_teal = rgb { 49 73 61 }
	nanru_nakar_med_teal = rgb { 109 152 162 }
	nanru_nakar_light_teal = rgb { 143 196 204 }
	nanru_nakar_blue = rgb { 0 136 173 }

	nathalaire_dark_grey	=	rgb { 30 30 35 }
	nathalaire_light_grey	=	rgb { 80 90 90 }

	nazhni_blue	=	rgb { 84 153 255 }
	nazhni_sky_blue	=	rgb { 201 251 255 }
	nazhni_gold	=	rgb { 255 190 78 }
	nazhni_navy	=	rgb { 0 19 27 }

	new_uanced_green = rgb { 105 200 130 }
	
	neratica_grey	=	rgb { 161 161 161 }
	neratica_dark_grey	=	rgb { 42 44 42 }
	neratica_black	=	rgb { 35 35 35 }
	neratica_red	=	rgb { 183 52 54 }

	obsidian_pink = rgb { 80 31 128 }
	obsidian_purple = rgb { 92 36 148 }

	orlghelovar_blue = rgb { 18 34 108 }
	orlghelovar_teal = rgb { 0 204 255 }
	orlghelovar_pearl = rgb { 172 241 255 }

	pearlsedge_blue	=	rgb { 46 49 146 }
	pearlsedge_pearl	=	rgb { 254 248 223 }

	quitl_yellow	=	rgb { 255 246 164 }

	revrhavn_blue = rgb { 112 125 175 }

	roilsardi_red	=	rgb { 185 30 68 }
	roilsardi_green	=	rgb { 176 229 104 }
	
	saamirses_purple = rgb { 156 72 115 }
	saamirses_pink = rgb { 213 98 117 }
	saamirses_blue = rgb { 72 208 253 }
	saamirses_dark_blue = rgb { 0 124 165 }
	saamirses_gold = rgb { 255 159 29 }
	
	sarda_blue	=	rgb { 10 90 165 }
	sarda_teal	=	rgb { 0 209 255 }
	
	seghdihr_blue =	rgb { 36 96 143 }

	sglard_beige = rgb { 236 225 166 }
	
	shazstundihr_grey	=	rgb { 96 96 96 }
	shazstundihr_brown	=	rgb { 159 151 134 }
	
	silverforge_blue	=	rgb { 193 216 224 }
	silverforge_grey	=	rgb { 58 58 60 }
	silverforge_silver	=	rgb { 209 210 212 }
	
	sirtan_beige =	rgb { 193 129 93 }
	sirtan_yellow =	rgb { 187 159 0 }
	sirtan_brown =	rgb { 108 39 0 }

	small_blue	=	rgb { 30 75 125 }
	small_green	=	rgb { 10 102 49 }
	small_red	=	rgb { 183 23 31 }
	small_yellow	=	rgb { 216 191 23 }
	small_orange	=	rgb { 214 86 32 }
	small_purple_light	=	rgb { 143 85 163 }
	small_purple_dark	=	rgb { 77 42 124 }

	soyzkaru_red = rgb { 119 9 0 }

	themaria_blue = rgb { 104 205 236 }
	themaria_grey = rgb { 80 80 80 }

	tianlou_grey = rgb { 178 178 178 }

	tibokbo_maroon = rgb { 60 0 74 }
	tibokbo_light_purple = rgb { 144 95 208 }
	tibokbo_eggshell = rgb { 229 234 164 }
	tibokbo_purple = rgb { 60 22 159 }
	tibokbo_green = rgb { 167 229 83 }

	tiru_moine_gold = rgb { 255 201 53 }
	tiru_moine_yellow = rgb { 251 237 117 }
	tiru_moine_blue = rgb { 164 210 226 }

	tungr_dark_blue	=	rgb { 37 67 91 }
	tungr_medium_blue	=	rgb { 55 91 119 }
	tungr_light_blue	=	rgb { 73 118 147 }
	tungr_copper	=	rgb { 173 100 56 }

	tvarateram_black	=	rgb { 5 37 71 }
	tvarateram_yellow = rgb { 255 251 28 }
	tvarateram_orange	=	rgb { 252 108 5 }

	udzenklan_dark_green	=	rgb { 9 31 16 }

	umoji_baashidi_dark_blue	=	rgb { 0 23 115 }
	umoji_baashidi_light_blue	=	rgb { 2 103 211 }
	umoji_baashidi_seafoam	=	rgb { 0 252 168 }
	umoji_baashidi_yellow	=	rgb { 255 255 0 }

	unguldavor_dark_green	=	rgb { 73 123 85 }
	unguldavor_light_green	=	rgb { 211 253 211 }

	vanburian_red	=	rgb { 132 38 39 }
	vanburian_grey	=	rgb { 167 169 172 }
	
	vanrahar_red	=	rgb { 55 0 5 }
	
	vels_bacar_aqua	=	rgb { 136 244 235 }
	vels_bacar_blue	=	rgb { 0 100 235 }
	
	vels_fadhecai_pink	=	rgb { 255 76 255 }

	verkal_gulan_gold	=	rgb { 255 226 71 }
	verkal_gulan_black	=	rgb { 24 26 40 }
	
	verne_red	=	rgb { 150 0 0 } #Verne's color in EU4
	verne_wyvern_red	=	rgb { 125 49 40 }
	verne_beige	=	rgb { 194 181 155 }
	
	vyceda_blue	=	rgb { 64 88 160 }

	wexonard_purple	=	rgb { 97 0 137 }

	yanshen_blue = rgb { 69 98 224 }
	yanshen_red = rgb { 212 46 46 }

	ynn_red	=	rgb { 123 27 5 }
	ynn_gold	=	rgb { 255 172 64 }

	ynngard_brown	=	rgb { 174 172 149 }
	ynngard_yellow	=	rgb { 253 255 198 }
	ynngard_green	=	rgb { 155 227 159 }

	zerzeko_green	=	rgb { 43 96 68 }

	zhiqian_grey	=	rgb { 214 214 214 }
	zhiqian_blue	=	rgb { 11 17 69 }

	zyujyut_gold	=	rgb { 254 169 5 }
	zyujyut_red	=	rgb { 195 42 30 }
	zyujyut_dark_red	=	rgb { 89 14 8 }
	
	beige	=	rgb { 155 129 108 }
	beige_light	=	rgb { 190 160 135 }
	brown_dark	=	rgb { 40 24 15}
	cream	=	rgb { 224 213 175 }
	lime	=	rgb { 150 180 80 }
	mud	=	rgb { 60 70 0 }
	purpure	=	rgb { 133 23 69 }
	pink_light	=	rgb { 250 180 210 }
	sapphire	=	rgb { 50 70 140 }
	silver	=	rgb { 172 180 196 }
	turquoise	=	rgb { 34 165 178 }
	
	malachite	=	rgb { 4 73 55 }

	konolkhatep_red = rgb { 85 0 0}
	melakmengi_gold = rgb { 255 229 0}
	melakmengi_brown = rgb { 233 147 0}
	melakmengi_blue = rgb { 20 158 182 }

	arawkelin_gold = rgb { 250 211 11 }
	arawkelin_blue = rgb { 30 66 155 }
	arawkelin_red = rgb { 209 32 39}
} 