﻿state_archmages_investment_pool_contribution_add={
	decimals=0
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

state_archmages_investment_pool_efficiency_mult={
	decimals=0
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_archmages_shares_add={
	decimals=0
	color=neutral
	game_data={
		ai_value=0
	}
}

