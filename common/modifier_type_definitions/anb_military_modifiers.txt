﻿unit_combat_unit_type_irregular_infantry_offense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}

unit_combat_unit_type_line_infantry_offense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}

unit_combat_unit_type_skirmish_infantry_offense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}

unit_combat_unit_type_irregular_infantry_defense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_line_infantry_defense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_skirmish_infantry_defense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_undead_infantry_offense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_undead_infantry_defense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_hussars_offense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_hussars_defense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_dragoons_offense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_dragoons_defense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_cuirassiers_offense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_cuirassiers_defense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_lancers_offense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_lancers_defense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_turret_cavalry_offense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_turret_cavalry_defense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}

unit_combat_unit_type_cannon_artillery_offense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_mobile_artillery_offense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}
unit_combat_unit_type_shrapnel_artillery_offense_add = {
	decimals=0
	color=good
	game_data={
		ai_value=0
	}
}