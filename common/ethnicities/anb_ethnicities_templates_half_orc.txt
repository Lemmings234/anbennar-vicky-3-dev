﻿# Receives data from /genes
# Ethnicity templates are used in other ethnicities and templates in this folder


#Anbennar - until we get tusks these guys will just use orcish but have less chance of being bald, plus more hair color variety
half_orc_cannorian={ 

    template = "gray_orc"

	skin_color = {
		10 = { 0.0 0.0 0.5 0.12 }   #caucasian skin
        10 = { 0.4 0.9 0.45 0.95 }    #LIGHT foresty green
        10 = { 0.74 0.85 0.78 0.95 }  #LIGHT grays
	}

	eye_color = {
		# Blue
		10 = { 0.65 0.05 1.0 0.6 }
		# Green
		10 = { 0.5 0.05 0.65 0.6 }
		# Brown
		10 = { 0.1 0.2 0.4 0.8 }
	}
	hair_color = {
		# Blonde
		10 = { 0.4 0.25 0.75 0.5 }
		# Brown
		30 = { 0.65 0.5 0.9 0.99 }
		# Red
		10 = { 0.85 0.0 0.99 0.5 }
		# Black
		40 = { 0.0 0.9 0.2 0.99 }
	}

	hairstyles = {
		10 = { name = european_hairstyles 		range = { 0.0 1.0 } }
        10 = { name = no_hairstyles 		range = { 0.0 1.0 } }
	}

	beards = {
		10 = { name = european_beards 		    range = { 0.0 1.0 } }
        20 = { name = no_beard                      range = { 0.0 1.0 } }
	}

	mustaches = {
		10 = { name = european_mustaches 		range = { 0.0 1.0 } }
        20 = { name = no_mustache       range = { 0.0 1.0 } }
	}

}

half_orc_grombari={ 

    template = "gray_orc"

	skin_color = {
        10 = { 0.74 0.85 0.78 0.95 }  #LIGHT grays
	}

	eye_color = {
		# Blue
		20 = { 0.65 0.05 1.0 0.6 }  #higher chance of blue due to gerudian parentagw
		# Green
		10 = { 0.5 0.05 0.65 0.6 }
		# Brown
		10 = { 0.1 0.2 0.4 0.8 }
	}
	hair_color = {
		# Blonde
		20 = { 0.4 0.25 0.75 0.5 }  #higher chance of blonde due to gerudian
		# Brown
		10 = { 0.65 0.5 0.9 0.99 }
		# Red
		10 = { 0.85 0.0 0.99 0.5 }
		# Black
		40 = { 0.0 0.9 0.2 0.99 }
	}

	hairstyles = {
		10 = { name = european_hairstyles 		range = { 0.0 1.0 } }
        10 = { name = no_hairstyles 		range = { 0.0 1.0 } }
	}

	beards = {
		10 = { name = european_beards 		    range = { 0.0 1.0 } }
        20 = { name = no_beard                      range = { 0.0 1.0 } }
	}

	mustaches = {
		10 = { name = european_mustaches 		range = { 0.0 1.0 } }
        20 = { name = no_mustache       range = { 0.0 1.0 } }
	}

}