﻿dna_mheromar_malartak={
	portrait_info = {
	type=female
	age=0.200000
	genes={ 		
		hair_color={ 3 252 255 18 }
 		skin_color={ 208 249 208 249 }
 		eye_color={ 255 0 50 247 }
 		gene_cheek_fat={ "cheek_fat" 90 "cheek_fat" 47 }
 		gene_cheek_forward={ "cheek_forward" 95 "cheek_forward" 134 }
 		gene_cheek_height={ "cheek_height" 121 "cheek_height" 171 }
 		gene_cheek_prom={ "cheek_prom" 132 "cheek_prom" 169 }
 		gene_cheek_width={ "cheek_width" 133 "cheek_width" 133 }
 		gene_chin_forward={ "chin_forward" 124 "chin_forward" 121 }
 		gene_chin_height={ "chin_height" 145 "chin_height" 136 }
 		gene_chin_width={ "chin_width" 86 "chin_width" 35 }
 		gene_ear_angle={ "ear_angle" 94 "ear_angle" 175 }
 		gene_ear_inner_shape={ "ear_inner_shape" 137 "ear_inner_shape" 137 }
 		gene_ear_lower_bend={ "ear_lower_bend" 27 "ear_lower_bend" 27 }
 		gene_ear_out={ "ear_out" 96 "ear_out" 179 }
 		gene_ear_size={ "ear_size" 123 "ear_size" 131 }
 		gene_ear_upper_bend={ "ear_upper_bend" 132 "ear_upper_bend" 132 }
 		gene_eye_angle={ "eye_angle" 128 "eye_angle" 127 }
 		gene_eye_depth={ "eye_depth" 146 "eye_depth" 167 }
 		gene_eye_distance={ "eye_distance" 139 "eye_distance" 139 }
 		gene_eye_height={ "eye_height" 104 "eye_height" 129 }
 		gene_eye_shut={ "eye_shut" 124 "eye_shut" 114 }
 		gene_eye_corner_def={ "eye_corner_def" 179 "eye_corner_def" 125 }
 		gene_eye_corner_depth_min={ "eye_corner_depth_min" 84 "eye_corner_depth_min" 86 }
 		gene_eye_fold_droop={ "eye_fold_droop" 110 "eye_fold_droop" 175 }
 		gene_eye_fold_shape={ "eye_fold_shape" 119 "eye_fold_shape" 159 }
 		gene_eye_size={ "eye_size" 122 "eye_size" 127 }
 		gene_eye_upper_lid_size={ "eye_upper_lid_size" 146 "eye_upper_lid_size" 126 }
 		gene_forehead_angle={ "forehead_angle" 151 "forehead_angle" 106 }
 		gene_forehead_brow_curve={ "forehead_brow_curve" 198 "forehead_brow_curve" 130 }
 		gene_forehead_brow_forward={ "forehead_brow_forward" 158 "forehead_brow_forward" 130 }
 		gene_forehead_brow_height={ "forehead_brow_height" 98 "forehead_brow_height" 127 }
 		gene_forehead_brow_inner_height={ "forehead_brow_inner_height" 151 "forehead_brow_inner_height" 120 }
 		gene_forehead_brow_outer_height={ "forehead_brow_outer_height" 169 "forehead_brow_outer_height" 227 }
 		gene_forehead_brow_width={ "forehead_brow_width" 136 "forehead_brow_width" 136 }
 		gene_forehead_height={ "forehead_height" 187 "forehead_height" 117 }
 		gene_forehead_roundness={ "forehead_roundness" 119 "forehead_roundness" 131 }
 		gene_forehead_width={ "forehead_width" 122 "forehead_width" 122 }
 		gene_head_height={ "head_height" 165 "head_height" 134 }
 		gene_head_profile={ "head_profile" 142 "head_profile" 173 }
 		gene_head_top_height={ "head_top_height" 137 "head_top_height" 137 }
 		gene_head_top_width={ "head_top_width" 140 "head_top_width" 140 }
 		gene_head_width={ "head_width" 125 "head_width" 125 }
 		gene_jaw_angle={ "jaw_angle" 181 "jaw_angle" 139 }
 		gene_jaw_def={ "jaw_def" 210 "jaw_def" 215 }
 		gene_jaw_forward={ "jaw_forward" 105 "jaw_forward" 117 }
 		gene_jaw_height={ "jaw_height" 90 "jaw_height" 143 }
 		gene_jaw_width={ "jaw_width" 105 "jaw_width" 83 }
 		gene_mouth_corner_height={ "mouth_corner_height" 156 "mouth_corner_height" 122 }
 		gene_mouth_forward={ "mouth_forward" 131 "mouth_forward" 135 }
 		gene_mouth_height={ "mouth_height" 98 "mouth_height" 98 }
 		gene_mouth_lower_lip_def={ "mouth_lower_lip_def" 86 "mouth_lower_lip_def" 215 }
 		gene_mouth_lower_lip_full={ "mouth_lower_lip_full" 34 "mouth_lower_lip_full" 103 }
 		gene_mouth_lower_lip_pads={ "mouth_lower_lip_pads" 50 "mouth_lower_lip_pads" 127 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size" 73 "mouth_lower_lip_size" 125 }
 		gene_mouth_lower_lip_width={ "mouth_lower_lip_width" 100 "mouth_lower_lip_width" 100 }
 		gene_mouth_open={ "mouth_open" 132 "mouth_open" 132 }
 		gene_mouth_philtrum_curve={ "mouth_philtrum_curve" 99 "mouth_philtrum_curve" 134 }
 		gene_mouth_philtrum_def={ "mouth_philtrum_def" 113 "mouth_philtrum_def" 115 }
 		gene_mouth_philtrum_width={ "mouth_philtrum_width" 126 "mouth_philtrum_width" 125 }
 		gene_mouth_upper_lip_curve={ "mouth_upper_lip_curve" 39 "mouth_upper_lip_curve" 192 }
 		gene_mouth_upper_lip_def={ "mouth_upper_lip_def" 95 "mouth_upper_lip_def" 143 }
 		gene_mouth_upper_lip_full={ "mouth_upper_lip_full" 101 "mouth_upper_lip_full" 136 }
 		gene_mouth_upper_lip_width={ "mouth_upper_lip_width" 165 "mouth_upper_lip_width" 117 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size" 95 "mouth_upper_lip_size" 80 }
 		gene_mouth_width={ "mouth_width" 161 "mouth_width" 85 }
 		gene_nose_curve={ "nose_curve" 103 "nose_curve" 86 }
 		gene_nose_forward={ "nose_forward" 142 "nose_forward" 109 }
 		gene_nose_hawk={ "nose_hawk" 81 "nose_hawk" 93 }
 		gene_nose_height={ "nose_height" 50 "nose_height" 86 }
 		gene_nose_length={ "nose_length" 128 "nose_length" 128 }
 		gene_nose_nostril_angle={ "nose_nostril_angle" 99 "nose_nostril_angle" 27 }
 		gene_nose_nostril_height={ "nose_nostril_height" 123 "nose_nostril_height" 123 }
 		gene_nose_nostril_width={ "nose_nostril_width" 104 "nose_nostril_width" 93 }
 		gene_nose_ridge_angle={ "nose_ridge_angle" 129 "nose_ridge_angle" 129 }
 		gene_nose_ridge_def={ "nose_ridge_def" 129 "nose_ridge_def" 129 }
 		gene_nose_ridge_def_min={ "nose_ridge_def_min" 120 "nose_ridge_def_min" 120 }
 		gene_nose_ridge_width={ "nose_ridge_width" 113 "nose_ridge_width" 111 }
 		gene_nose_size={ "nose_size" 140 "nose_size" 80 }
 		gene_nose_tip_angle={ "nose_tip_angle" 105 "nose_tip_angle" 117 }
 		gene_nose_tip_forward={ "nose_tip_forward" 200 "nose_tip_forward" 137 }
 		gene_nose_tip_width={ "nose_tip_width" 10 "nose_tip_width" 96 }
 		gene_neck_length={ "neck_length" 168 "neck_length" 168 }
 		gene_neck_width={ "neck_width" 95 "neck_width" 95 }
 		gene_bs_body_type={ "body_fat_head_fat_low_elf" 114 "body_fat_head_fat_low_elf" 114 }
 		gene_height={ "normal_height" 255 "normal_height" 255 }
 		gene_age={ "no_aging" 26 "no_aging" 26 }
 		gene_old_ears={ "old_ears_elf" 0 "old_ears_elf" 0 }
 		gene_old_eyes={ "old_eyes_elf" 16 "old_eyes_elf" 16 }
 		gene_old_forehead={ "old_forehead_elf" 19 "old_forehead_elf" 19 }
 		gene_old_mouth={ "old_mouth_elf" 4 "old_mouth_elf" 4 }
 		gene_old_nose={ "old_nose_elf" 14 "old_nose_elf" 14 }
 		gene_complexion={ "complexion_04" 255 "complexion_04" 0 }
 		gene_stubble={ "stubble_low" 0 "stubble_low" 0 }
 		gene_crowfeet={ "crowfeet_01" 0 "crowfeet_01" 0 }
 		gene_face_dacals={ "face_dacal_05" 255 "face_dacal_05" 255 }
 		gene_frown={ "frown_02" 13 "frown_02" 13 }
 		gene_surprise={ "surprise_02" 4 "surprise_02" 4 }
 		gene_eyebrows_shape={ "avg_spacing_avg_thickness_elf" 238 "avg_spacing_avg_thickness_elf" 238 }
 		gene_eyebrows_fullness={ "layer_2_avg_thickness_elf" 135 "layer_2_avg_thickness_elf" 135 }
 		hairstyles={ "elven_hairstyles" 0 "elven_hairstyles" 105 }
 		beards={ "no_beard" 14 "no_beard" 14 }
 		mustaches={ "no_mustache" 103 "no_mustache" 103 }
 		props={ "no_prop" 0 "no_prop" 0 }
 		eye_accessory={ "normal_eyes" 0 "normal_eyes" 0 }
 		eye_lashes_accessory={ "normal_eyelashes" 0 "normal_eyelashes" 0 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		POD_NOSschnoz={ "nosies" 0 "nosies" 0 }
 		skin_color_override={ "default_1" 127 "default_1" 127 }
 		race_gene_mer_ears_01={ "mer_ears_01" 255 "mer_ears_02" 172 }
 		race_gene_mer_ears_02={ "mer_ears_02" 255 "mer_ears_02" 203 }
	}
	}
	enabled=yes
}