﻿ai_strategy_reunify_command = {
	icon = "gfx/interface/icons/ai_strategy_icons/unification.dds"
	
	type = diplomatic
	
	undesirable_infamy_level = {
		value = 50
	}
	
	unacceptable_infamy_level = {
		value = 100
	}
	
	ideological_opinion_effect_mult = {
		value = 0.5
	}
	
	wargoal_maneuvers_fraction = {
		value = 0.1
	}		
		
	diplomatic_play_neutrality = {
		value = 0
	}
	
	diplomatic_play_boldness = {
		value = 50
	}
		
	obligation_value = {
		value = 0
	
		if = {
			limit = { 
				scope:target_country = { can_form_nation = R21 }
			}
			add = 25
		}
	}			
		
	recklessness = {
		value = 0.5
	}	
	
	aggression = {
		value = 0.5
		
		if = {
			limit = { 
				scope:target_country = { can_form_nation = R21 }
			}
			add = 2.5
		}

		# Less aggressive against countries we're just antagonistic against, rather than belligerent/domineering
		if = {
			limit = {
				has_attitude = {
					who = scope:target_country
					attitude = antagonistic
				}					
			}
			multiply = 0.25
		}

		# More aggressive against ideological enemies, a bit less aggressive against ideological allies
		if = {
			limit = {
				"ai_ideological_opinion(scope:target_country)" < 0
			}
			
			multiply = {
				value = "ai_ideological_opinion(scope:target_country)"
				multiply = -0.2
				add = 1
				max = 4.0
			}				
		}	
		else = {
			multiply = {
				value = "ai_ideological_opinion(scope:target_country)"
				multiply = -0.05
				add = 1
				min = 0.5
			}			
		}		
	}

	building_group_weights = {
		bg_army = 1.5
	}
	
	wargoal_scores = {
		annex_country = {
			if = {
				limit = {
					scope:target_country = { can_form_nation = R21 }
					can_reach_target_country = yes
				}
				add = 300
			}		
		}	
		conquer_state = {
			if = {
				limit = {
					scope:target_state = {				
						state_region = {
							OR = {
								is_homeland = cu:chimera_hobgoblin
								is_homeland = cu:dragon_hobgoblin
								is_homeland = cu:tiger_hobgoblin
								is_homeland = cu:elephant_hobgoblin
								is_homeland = cu:wuhyun_half_orc
							}		
						}
					}
					can_reach_target_state = yes
				}
				add = 100
			}
		}
		return_state = {
			if = {
				limit = {
					scope:target_state = {					
						state_region = {
							OR = {
								is_homeland = cu:chimera_hobgoblin
								is_homeland = cu:dragon_hobgoblin
								is_homeland = cu:tiger_hobgoblin
								is_homeland = cu:elephant_hobgoblin
								is_homeland = cu:wuhyun_half_orc
							}
						}	
					}
					can_reach_target_state = yes
				}
				add = 200
			}
		}		
	}			
	
	secret_goal_scores = {
		befriend = {		
			if = {
				limit = {
					scope:target_country = { can_form_nation = R21 }
				}
				add = 150
				
				if = {
					limit = {
						can_reach_target_country = no
					}	
					add = 150
				}	
			}
		}				
		conquer = {				
			if = {
				limit = {
					scope:target_country = { can_form_nation = R21 }
					can_reach_target_country = yes
				}
				add = 500
			}				
		}			
	}	

	possible = {
		NOT = { exists = c:R21 }
		can_form_nation = R21	
	}

	weight = {
		value = 50
		
		if = {
			limit = { NOT = { is_country_type = unrecognized } }
			add = 100
		}		
		
		if = {
			limit = { country_rank = rank_value:unrecognized_regional_power }
			add = 25
		}

		if = {
			limit = { country_rank = rank_value:unrecognized_major_power }
			add = 50
		}		
	}
}

ai_strategy_eordand_unification = {
 	icon = "gfx/interface/icons/ai_strategy_icons/unification.dds"
	
 	type = diplomatic
	
 	undesirable_infamy_level = {
 		value = 50
 	}
	
 	unacceptable_infamy_level = {
 		value = 100
 	}
	
 	ideological_opinion_effect_mult = {
 		value = 0.5
 	}
	
 	wargoal_maneuvers_fraction = {
 		value = 0.1
 	}		
		
 	diplomatic_play_neutrality = {
 		value = 0
 	}
	
 	diplomatic_play_boldness = {
 		value = 50
 	}
		
 	obligation_value = {
 		value = 0
	
 		if = {
			limit = { 
 				scope:target_country = { can_form_nation = B87 }
 			}
 			add = 50
		}
 	}			
		
 	recklessness = {
 		value = 0.5
 	}	
	
 	aggression = {
 		value = 0.5
		
 		if = {
 			limit = { 
 				scope:target_country = { #More aggressive against occupier
 					NOT = { any_primary_culture = { has_discrimination_trait = eordan } }
 					any_scope_state = {	region = sr:region_eordand	}
 				}
 			}
 			add = 4.5
 		}	
 	}

 	building_group_weights = {
 		bg_army = 1
 	}
	
 	wargoal_scores = {
 		liberate_subject = {
 			if = {
 				limit = {
 					scope:target_country = { this = c:B80 }
 				}
 				add = 500
 			}		
 		}	
 		conquer_state = {
 			if = {
 				limit = {
 					country_has_primary_culture = cu:selphereg
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:selphereg		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 100
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:caamas
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:caamas		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 100
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:peitar
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:peitar		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 100
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:tuathak
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:tuathak		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 100
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:snecboth
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:snecboth		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 100
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:fograc
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:fograc		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 100
 			}
 		}
 		take_treaty_port = {
 			if = {
 				limit = {
 					country_has_primary_culture = cu:selphereg
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:selphereg		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 100
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:caamas
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:caamas		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 100
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:peitar
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:peitar		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 100
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:tuathak
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:tuathak		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 100
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:snecboth
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:snecboth		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 100
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:fograc
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:fograc		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 100
 			}
 		}
 		return_state = {
 			if = {
 				limit = {
 					country_has_primary_culture = cu:selphereg
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:selphereg		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 200
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:caamas
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:caamas		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 200
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:peitar
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:peitar		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 200
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:tuathak
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:tuathak		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 200
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:snecboth
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:snecboth		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 200
 			}
 			if = {
 				limit = {
 					country_has_primary_culture = cu:fograc
 					scope:target_state = {				
 						state_region = {
 							is_homeland = cu:fograc		
 						}
 					}
 					can_reach_target_state = yes
 				}
 				add = 200
 			}
 		}		
 	}			
	
 	secret_goal_scores = {
 		befriend = {		
 			if = {
 				limit = {
 					scope:target_country = { can_form_nation = B87 }
 				}
 				add = 500
 			}
 		}				
 		conquer = {				
 			if = {
 				limit = {
 					scope:target_country = { More aggressive against occupier
 						NOT = { any_primary_culture = { has_discrimination_trait = eordan } }
 						any_scope_state = {	region = sr:region_eordand	}
 					}
 				}
 				add = 500
 			}				
 		}			
 	}	

 	possible = {
 		NOT = { exists = c:B87 }
 		can_form_nation = c:B87
 	}

 	weight = {
 		value = 1000
 		if = {
 			limit = { 
 				any_country = {
 					OR = {
 						has_diplomatic_pact = {
 							who = root
 							type = alliance
 						}
 						has_diplomatic_pact = {
 							who = root
 							type = defensive_pact
 						}
 					}
 					has_journal_entry = je_eordand_diplomatic_unification 
 				}
 			}
 			add = 5000
 		}	
 	}

 }