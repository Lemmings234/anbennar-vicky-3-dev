﻿company_swinthorpe = {
	icon = "gfx/interface/icons/company_icons/basic_food.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_manufacturing_light.dds"

	flavored_company = yes

	building_types = {
		building_food_industry
		building_wheat_farm
	}

	potential = {
		has_interest_marker_in_region = region_epednan_expanse
        OR = {
            country_has_primary_culture = cu:steadsman
            country_has_primary_culture = cu:dustman
            country_has_primary_culture = cu:horizon_elf
        }
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_PLUMSTEAD
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_PLUMSTEAD
			is_incorporated = yes
			any_scope_building = {
				is_building_type = building_food_industry
				level >= 3
			}
			any_scope_building = {
				is_building_type = building_wheat_farm
				level >= 3
			}
		}
	}

	prosperity_modifier = {
		state_radicals_from_sol_change_mult= -0.05
		state_loyalists_from_sol_change_mult = 0.05
	}

	ai_weight = {
		value = 3
	}	
}


company_arangreen = {
    icon = "gfx/interface/icons/company_icons/basic_shipyards.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_harbor_shipbuilding.dds"


	flavored_company = yes

	building_types = {
		building_shipyards
		building_trade_center
	}

	potential = {
		has_interest_marker_in_region = region_noruin
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_CESTIRMARK
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_CESTIRMARK
			is_incorporated = yes
			any_scope_building = {
				is_building_type = building_shipyards
				level >= 3
			}
			has_technology_researched = gantry_cranes
		}
	}

	prosperity_modifier = {
		country_convoys_capacity_mult = 0.15
	    state_building_port_max_level_add = 3
	}

	ai_weight = {
		value = 3
	}	
}

company_arms_vanbury = {
	icon = "gfx/interface/icons/company_icons/basic_munitions.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_manufacturing_heavy.dds"

	flavored_company = yes

	building_types = {
		building_steel_mills
		building_arms_industry
		building_doodad_manufacturies
	}

	potential = {
		c:B05 ?= this
	}

	possible = {
		c:B05 ?= this
		any_scope_state = {
			state_region = s:STATE_STEEL_BAY
			is_incorporated = yes
			any_scope_building = {
				is_building_type = building_steel_mills
				level >= 3
			}
			any_scope_building = {
				is_building_type = building_arms_industry 
				level >= 3
			}
		}
	}

	prosperity_modifier = {
		unit_navy_offense_mult = 0.1
		unit_army_offense_mult = 0.1
	}

	ai_weight = {
		value = 3
	}
}

company_drilling = { #general expanse drilling
	icon = "gfx/interface/icons/company_icons/basic_gold_mining.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_mining.dds"

	flavored_company = yes

	building_types = {
		building_gold_mine
		building_oil_rig
	}

	potential = {
		capital.region = sr:region_epednan_expanse
	}

	possible = {
		any_scope_state = {
			region = sr:region_epednan_expanse
			any_scope_building = {
				OR = {
					is_building_type = building_gold_mine
					is_building_type = building_oil_rig
				}
				level >= 5
			}
		}
	}

	prosperity_modifier = {
		country_resource_discovery_chance_mult = 0.25
		interest_group_ig_industrialists_pol_str_mult = 0.15
		state_radicals_from_sol_change_mult = 0.05
	}

	ai_weight = {
		value = 3
	}
}