﻿company_jasienes_daughters = { #Jasiene's daughters: Healers and Logisticians
	icon = "gfx/interface/icons/company_icons/custom_companies_placeholder.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_railways.dds"
	
	flavored_company = yes
	
	building_types = {  
		building_railway
		building_shipyards
		building_opium_plantation
		# Will need to add airship stuff to this
	}	
	
	potential = {
		country_has_state_religion = rel:the_jadd
	}
	
	possible = {
		OR = {
			any_scope_state = {
				is_incorporated = yes
				any_scope_building = {
					is_building_type = building_opium_plantation
					level >= 3
				}
			}
			any_scope_state = {
				is_incorporated = yes
				any_scope_building = {
					is_building_type = building_shipyards
					level >= 3
				}
			}
			any_scope_state = {
				is_incorporated = yes
				has_building = building_railway
				count >= 4
			}
		}
	}
	
	prosperity_modifier = {
		interest_group_ig_devout_pop_attraction_mult = 0.1
		state_infrastructure_mult = 0.05
		state_mortality_mult = -0.05
	}	
	
	ai_weight = {
		value = 3 # Higher base value for flavored companies
	}
}


company_greysheep_fashion_company = { #Greysheep Fashion Company: Highest Quality for Goblins Everywhere
	icon = "gfx/interface/icons/company_icons/custom_companies_placeholder.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_railways.dds"
	
	flavored_company = yes
	
	building_types = {  
		building_textile_mills
		building_silk_plantation
	}	
	
	potential = {
		has_interest_marker_in_region = region_bahar
	}
	
	possible = {
		any_scope_state = {
			state_region = s:STATE_AQATBAHAR
			any_scope_building = {
				is_building_type = building_textile_mills
				level >= 5
			}
		}
	}
	
	prosperity_modifier = {
		country_influence_mult = 0.15
		country_prestige_mult = 0.05
	}	
	
	ai_weight = {
		value = 3 # Higher base value for flavored companies
	}
}