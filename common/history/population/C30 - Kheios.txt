﻿POPULATION = { #Kheionai
	c:C30 = { #Kheios
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_very_high = yes
	}
	
	c:C31 = { #Anisikheion
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_high = yes
	}
	
	c:C32 = { #The_Devand
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_high = yes
	}
	
	c:C33 = { #Keyelion
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_high = yes
	}
	
	c:C34 = { #Eneion
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_high = yes
	}
	
	c:C35 = { #Besolaki
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_high = yes
	}
	
	c:C36 = { #Apikhoxi
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_high = yes
	}
	
	c:C37 = { #Deyeion
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_high = yes
	}
	
	c:C38 = { #Mteibhara
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:C39 = { #Ameion
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_very_high = yes
	}
	
	
	c:C47 = { #Nymbhava
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_middling = yes
	}
	
	c:C50 = { #Degithion
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_middling = yes
	}
	
	c:C53 = { #Nisae
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_middling = yes
	}
	
	c:C55 = { #Amastol
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_middling = yes
	}
	
	c:C56 = { #Mesion
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_middling = yes
	}
	
	c:C57 = { #Bukkabeso
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_middling = yes
	}
	
	c:C59 = { #Degolidir
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_high = yes
	}
	
	c:C60 = { #Neor Empkeios
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_high = yes
	}
	
	c:C61 = { #Akretirchel
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:C70 = { #Amgremos
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_high = yes
	}
}