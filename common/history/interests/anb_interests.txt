﻿INTERESTS = {
	c:A01 ?= { #Anbennar
		add_declared_interest = region_adenica
		add_declared_interest = region_dostanor
		add_declared_interest = region_amadia
		add_declared_interest = region_north_lencenor
		add_declared_interest = region_castanor
	}
	
	c:A02 ?= { #Vivin Empire - Just regions in their general vicinity
		add_declared_interest = region_the_marches
		add_declared_interest = region_bahar
		add_declared_interest = region_businor
		add_declared_interest = region_adenica
		add_declared_interest = region_the_reach
		#add_declared_interest = region_eastern_dameshead #redundant
	}
	
	c:A03 ?= { #Lorent
		add_declared_interest = region_northern_dameshead
		#add_declared_interest = region_dragon_coast	# buggy
		add_declared_interest = region_eastern_dameshead
		add_declared_interest = region_businor
		#add_declared_interest = region_south_rahen		#redundant
		add_declared_interest = region_thidinkai
		add_declared_interest = region_lupulan
	}

	c:A04 ?= { #Northern League
		add_declared_interest = region_bulwar_proper
		add_declared_interest = region_adenica
		add_declared_interest = region_north_lencenor
	}

	c:A06 ?= { #Gnomish Hierarchy
		add_declared_interest = region_amadia
	}
	
	c:A09 ?= { #Busilar
		add_declared_interest = region_upper_fangaula
		add_declared_interest = region_dao_nako
		add_declared_interest = region_bulwar_proper
		add_declared_interest = region_harpy_hills
		add_declared_interest = region_kheterata
		add_declared_interest = region_jasiir_jadid
	}
	
	c:A10 ?= { #Grombar
		add_declared_interest = region_alen
		add_declared_interest = region_dragon_coast
		add_declared_interest = region_south_lencenor
		add_declared_interest = region_adenica
		add_declared_interest = region_triunic_lakes
		add_declared_interest = region_south_plains
		add_declared_interest = region_amadia
		add_declared_interest = region_eastern_dameshead
		add_declared_interest = region_businor
	}

	c:A25 ?= { #Araionn
		add_declared_interest = region_the_marches
	}

	c:A30 ?= { #Magocratic Demesne
		#add_declared_interest = region_adenica	#redundant
		add_declared_interest = region_the_marches
	}

	c:B98 ?= { #Trollsbay
		add_declared_interest = region_haraf
		add_declared_interest = region_tor_nayyi
		add_declared_interest = region_lower_ynn
		add_declared_interest = region_epednan_expanse
		add_declared_interest = region_dalaire
	}

	c:B49 ?= { #Dragon Dominion
		add_declared_interest = region_broken_sea
	}

	c:B29 ?= { #Sarda Empire
		add_declared_interest = region_upper_ynn
	}

	c:B27 ?= { #Neratica
		add_declared_interest = region_epednan_expanse
		#add_declared_interest = region_haraf	#redundant
	}

	c:B91 ?= { #Ranger Republic
		add_declared_interest = region_upper_ynn
	}

	c:B07 ?= { #Triarchy
		add_declared_interest = region_amadia
	}

	c:F01 ?= { #Jaddanzar
		add_declared_interest = region_bahar
		add_declared_interest = region_harpy_hills
		add_declared_interest = region_north_rahen
		add_declared_interest = region_south_plains
	}

	c:F02 ?= { #Surakesi League
		add_declared_interest = region_bahar
	}

	c:R72 ?= { #Dhugajir
		add_declared_interest = region_kharunyana
	}

	c:R08 ?= { #Soldiers Republic
		add_declared_interest = region_north_yanshen
	}


	c:D02 ?= { #Krakdhumvror
		add_declared_interest = region_kings_rock
	}
	c:D05 ?= { #Amldihr
		add_declared_interest = region_northern_pass
		add_declared_interest = region_mountainheart
	}

	c:D08 ?= { #Khugdihr
		add_declared_interest = region_castanor
	}
	
	c:D18 ?= { #Arg Ordstun
		add_declared_interest = region_bahar
		add_declared_interest = region_mountainheart
	}

	c:D25 ?= { #Verkal Gulan
		add_declared_interest = region_south_rahen
	}

	c:D26 ?= { #Nizhn Korvesto
		add_declared_interest = region_jade_mines
	}

	c:D31 ?= { #Dakaz Carzviya
		add_declared_interest = region_jade_mines
		add_declared_interest = region_north_rahen
	}

	c:D33 ?= { #Zerzeko Radin
		add_declared_interest = region_north_rahen
	}

	c:L01 ?= { #Konolkhatep
		add_declared_interest = region_upper_fangaula
		add_declared_interest = region_dao_nako
		add_declared_interest = region_shadowswamp
		add_declared_interest = region_taneyas
		add_declared_interest = region_bulwar_proper
		add_declared_interest = region_businor
	}


	c:L17 ?= { #Hakhepra
		add_declared_interest = region_taneyas
	}

	c:L04 ?= { #Khasa
		add_declared_interest = region_western_dameshead
	}

	c:L35 ?= { #Karshyr
		add_declared_interest = region_fahvanosy
	}

	c:L36 ?= { #Nazhni
		add_declared_interest = region_south_rahen
		add_declared_interest = region_lai_peninsula
	}
}
