﻿CHARACTERS = {
	c:B38 ?= {
		create_character = { #
			first_name = Elranjan
			last_name = yen_Amac
			historical = yes
			ruler = yes
			age = 62
			interest_group = ig_landowners
			ideology = ideology_traditionalist
			traits = {
				imperious bigoted scarred
			}
		}
	}
}
