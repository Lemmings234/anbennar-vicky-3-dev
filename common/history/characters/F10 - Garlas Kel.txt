﻿CHARACTERS = {
	c:F10 ?= {
		create_character = {
			template = ruler_nuzha_genie
			on_created = {
				add_character_role = general
				set_character_immortal = yes
			}
		}
	}
}