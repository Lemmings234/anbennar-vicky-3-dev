﻿CHARACTERS = {
	c:B27 ?= {
		create_character = {
			first_name = Emil
			last_name = sil_Onyx
			historical = yes
			ruler = yes
			age = 39
			is_general = yes
			interest_group = ig_armed_forces
			ideology = ideology_royalist
			traits = {
				wrathful ambitious experienced_offensive_planner
			}
		}
		create_character = {
			age = 47
			first_name = Denar
			last_name = sil_Arca_Cenad
			historical = yes
			interest_group = ig_devout
			ig_leader = yes
			ideology = ideology_theocrat
			traits = {
				imperious persistent
			}
		}
		create_character = {
			age = 36
			first_name = Elro
			historical = yes
			culture = cheshoshi
			interest_group = ig_armed_forces
			ig_leader = yes
			ideology = ideology_royalist
			traits = {
				honorable brave
			}
		}
		create_character = {
			age = 42
			first_name = Ultar
			historical = yes
			culture = cheshoshi
			interest_group = ig_armed_forces
			is_general = yes
			ideology = ideology_moderate
			traits = {
				direct innovative
			}
		}
	}
}
