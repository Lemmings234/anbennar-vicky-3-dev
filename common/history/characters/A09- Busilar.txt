﻿CHARACTERS = {
	c:A09 ?= {
		create_character = {
			first_name = "Ansaro"
			last_name = "Silnara"
			historical = yes
			age = 48
			culture = cu:busilari
			interest_group = ig_landowners
			ruler = yes
			ideology = ideology_royalist
			traits = {
				kidney_stones
				expensive_tastes
				sickly
				charismatic
				romantic
			}
		}
		create_character = {
			first_name = "Margotta"
			last_name = "Silnara"
			historical = yes
			age = 2
			heir = yes
			culture = cu:busilari
			interest_group = ig_industrialists
			female = yes
			ideology = ideology_royalist
			traits = {
				charismatic
			}
		}
	}
}
