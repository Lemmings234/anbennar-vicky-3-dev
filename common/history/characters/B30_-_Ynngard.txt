﻿CHARACTERS = {
	c:B34 ?= {
		create_character = { # longtime merc with ties to ynnic nobility, chosen to lead for his political alignment more than anything. Is worried about the rise of the Ranger Republic, but none have listened to his concerns yet. Is not fond of the separation of church and state that exists in ynngard.
			first_name = Varok_Yen
			last_name = Sardobnn
			historical = yes
			age = 57
			ruler = yes
			interest_group = ig_armed_forces
			ig_leader = yes
			commander = yes
			ideology = ideology_traditionalist
			traits = {
				direct romantic stalwart_defender
			}
		}
	}
}
