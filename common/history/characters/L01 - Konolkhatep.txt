﻿CHARACTERS = {
	c:L01 ?= {
		create_character = {
			template = ruler_horutep
			on_created = {
				add_character_role = general
				set_character_immortal = yes
			}
		}
	}
}
