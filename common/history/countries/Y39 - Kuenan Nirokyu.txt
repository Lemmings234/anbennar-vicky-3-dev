﻿COUNTRIES = {
	c:Y39 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		effect_starting_politics_traditional = yes

		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_magocracy
		activate_law = law_type:law_artifice_banned
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_local_tolerance 
		activate_law = law_type:law_censorship
		activate_law = law_type:law_appointed_bureaucrats

		add_technology_researched = urban_planning
	}
}