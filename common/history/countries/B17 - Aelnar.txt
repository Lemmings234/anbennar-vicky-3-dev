﻿COUNTRIES = {
	c:B17 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = tradition_of_equality
		add_technology_researched = nationalism
		add_technology_researched = mass_communication
		effect_starting_politics_reactionary = yes
		activate_law = law_type:law_magocracy #rule of Lithiel/Elissa
		activate_law = law_type:law_closed_borders
		activate_law = law_type:law_isolationism
		
		activate_law = law_type:law_ethnostate
		activate_law = law_type:law_women_own_property
		

		set_institution_investment_level = {
			institution = institution_police
			level = 2
		}
	}
}