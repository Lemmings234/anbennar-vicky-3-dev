﻿COUNTRIES = {
	c:B37 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = napoleonic_warfare
		add_technology_researched = empiricism
		add_technology_researched = tradition_of_equality

		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		# No home affairs
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_frontier_colonization
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_no_health_system
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_local_tolerance
		activate_law = law_type:law_nation_of_artifice

		set_institution_investment_level = { #Expanse countries have low pop and need it to colonize at a decent speed
			institution = institution_colonial_affairs
			level = 2
		}

        add_journal_entry = {
            type = je_ranger_coup_response
        }
	}
}