﻿COUNTRIES = {
	c:Y33 ?= {
		effect_starting_technology_tier_2_tech = yes
		
		effect_starting_politics_traditional = yes
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_freedom_of_conscience
	}
}