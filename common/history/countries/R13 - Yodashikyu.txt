﻿COUNTRIES = {
	c:R13 ?= {
		effect_starting_technology_tier_4_tech = yes

		add_technology_researched = mandatory_service
		add_technology_researched = line_infantry
		
		activate_law = law_type:law_stratocracy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_no_police
		activate_law = law_type:law_religious_schools
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property #Women in workplace not possible without feminism
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_debt_slavery
		
		activate_law = law_type:law_expanded_tolerance
		
		
		activate_law = law_type:law_nation_of_artifice

		add_taxed_goods = g:tea

		add_modifier = {
			name = hobgoblin_statocracy_modifier
		}
	}
}