﻿COUNTRIES = {
	c:R02 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		add_technology_researched = cotton_gin
		add_technology_researched = lathe
		add_technology_researched = mandatory_service
		add_technology_researched = urban_planning
		add_technology_researched = law_enforcement
		add_technology_researched = academia
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_national_militia
		
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_local_police
		activate_law = law_type:law_religious_schools
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_women_own_property
		
		activate_law = law_type:law_expanded_tolerance
		
		
		activate_law = law_type:law_nation_of_artifice

		add_journal_entry = { type = je_nasavanite_economic_philosophy }
	}
}