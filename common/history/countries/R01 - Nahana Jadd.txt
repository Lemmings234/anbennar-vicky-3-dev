﻿COUNTRIES = {
	c:R01 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		activate_law = law_type:law_theocracy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_cultural_exclusion #Multiculturalism not possible without human rights
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_no_police
		activate_law = law_type:law_no_schools #Religious schools not possible with serfdom
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_women_own_property
		
		activate_law = law_type:law_expanded_tolerance
		
		
		activate_law = law_type:law_nation_of_magic
	}
}