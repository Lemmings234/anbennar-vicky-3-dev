﻿COUNTRIES = {
	c:A16 ?= {
		effect_starting_technology_tier_1_tech = yes
		effect_starting_artificery_tier_2_tech = yes
		add_technology_researched = tradition_of_equality
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_hereditary_bureaucrats	
		activate_law = law_type:law_national_militia
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_protectionism
		activate_law = law_type:law_consumption_based_taxation
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_right_of_assembly
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_same_heritage_only # human supremacy
		
			#one of few that start with this
		activate_law = law_type:law_nation_of_artifice	
	}
}