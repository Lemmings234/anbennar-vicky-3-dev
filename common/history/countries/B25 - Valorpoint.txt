﻿COUNTRIES = {
	c:B25 ?= {
		effect_starting_technology_tier_2_tech = yes

		add_technology_researched = egalitarianism

		effect_starting_politics_liberal = yes
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_elected_bureaucrats	
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_guaranteed_liberties
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		# No colonial affairs
		# No police force
		activate_law = law_type:law_religious_schools
		# No health system
		activate_law = law_type:law_nation_of_magic
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		# No migration controls
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_expanded_tolerance

		ig:ig_devout = {
			add_ruling_interest_group = yes
		}

		ig:ig_rural_folk = {
			add_ruling_interest_group = yes #Pacifist faction in power after the loss of southern Valorpoint
		}
	}
}