﻿COUNTRIES = {
	c:F02 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = tradition_of_equality
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_wealth_voting
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_national_militia
		
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_local_police
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_women_own_property
		
		activate_law = law_type:law_local_tolerance
		
		
		activate_law = law_type:law_nation_of_magic
	}
}