﻿COUNTRIES = {
	c:Y43 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		effect_starting_politics_traditional = yes
		activate_law = law_type:law_technocracy
		activate_law = law_type:law_appointed_bureaucrats
		
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_censorship
	}
}