﻿COUNTRIES = {
	c:B86 ?= {
		effect_starting_technology_tier_6_tech = yes
        add_modifier = {
			name = native_gladewarden_modifier
			months = -1
		}
		
		add_technology_researched = tradition_of_equality
		
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_national_guard
	}
}