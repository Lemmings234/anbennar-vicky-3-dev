﻿COUNTRIES = {
	c:B39 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = academia
		add_technology_researched = urban_planning
		add_technology_researched = law_enforcement
		add_technology_researched = paddle_steamer #gnomish submarine tech

		effect_starting_politics_traditional = yes

		activate_law = law_type:law_theocracy
		activate_law = law_type:law_oligarchy #patricians control the priests
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_censorship
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_local_tolerance
		activate_law = law_type:law_artifice_banned
		 #Bloodrunes

		set_ruling_interest_groups = {
			ig_landowners
		}
	}
}
