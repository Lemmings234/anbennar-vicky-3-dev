﻿COUNTRIES = {
	c:B47 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = academia
		add_technology_researched = urban_planning
		add_technology_researched = law_enforcement

		effect_starting_politics_traditional = yes

		activate_law = law_type:law_oligarchy #Feudal nobility
		
		activate_law = law_type:law_racial_segregation #Nestrin's policies of Ynnic-Eordan unity against cannorians
		activate_law = law_type:law_censorship
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_local_tolerance
		activate_law = law_type:law_artifice_banned
		 #Bloodrunes

		set_ruling_interest_groups = {
			ig_landowners
		}

        add_journal_entry = {
            type = je_ranger_coup_response
        }
	}
}
