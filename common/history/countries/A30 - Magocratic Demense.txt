﻿COUNTRIES = {
	c:A30 ?= {
		effect_starting_technology_tier_3_tech = yes	#doc says lagging behind rest of Escann in terms of institutions and economy
		
		activate_law = law_type:law_magocracy	#absolutist monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_traditionalism # Not allowed interventionism with serfdom
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		
		activate_law = law_type:law_censorship #Outlawed dissent requires political agitation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_artifice_banned
	}
}