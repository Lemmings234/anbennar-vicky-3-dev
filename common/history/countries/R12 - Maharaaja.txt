﻿COUNTRIES = {
	c:R12 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_land_based_taxation
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_local_tolerance
		
		
		activate_law = law_type:law_traditional_magic_banned
	}
}