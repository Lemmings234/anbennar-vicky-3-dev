﻿COUNTRIES = {
	c:A14 ?= {
		effect_starting_technology_tier_2_tech = yes
		add_technology_researched = intensive_agriculture # So they can use all of GHs excess fertilizer
		add_technology_researched = lathe # They start with dye, they should be able to use it, and its a very low tech for their region
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_national_militia
		
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_mercantilism	#exporting grain
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_same_heritage_only
		
		activate_law = law_type:law_nation_of_magic	

		#Consumption-based Taxation
		add_taxed_goods = g:grain
		add_taxed_goods = g:tobacco	#weed tax bruh
		add_taxed_goods = g:services

		ig:ig_rural_folk = {
			add_ruling_interest_group = yes
		}

		ig:ig_landowners = { 
			add_ruling_interest_group = yes
		}
	}
}