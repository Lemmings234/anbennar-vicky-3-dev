﻿POPS = {
	s:STATE_AVHAVUBHIYA = {
		region_state:R20 = {
			create_pop = {
				culture = raghamidesh
				size = 6113000
			}
			create_pop = {
				culture = surani
				size = 62000
			}
			create_pop = {
				culture = royal_harimari
				size = 370000
				religion = the_jadd
			}
			create_pop = {
				culture = sun_elven
				size = 70000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 150000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 70000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 370000
			}

		}
	}
	s:STATE_IYARHASHAR = {
		region_state:R20 = {
			create_pop = {
				culture = raghamidesh
				size = 1560000
			}
			create_pop = {
				culture = royal_harimari
				size = 1410000
				split_religion = {
					royal_harimari = {
						the_jadd = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = sun_elven
				size = 30000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 60000
			}

		}
	}
	s:STATE_SOUTH_GHANKEDHEN = {
		region_state:R02 = {
			create_pop = {
				culture = ghankedhen
				size = 3462000
			}
			create_pop = {
				culture = pasindesh
				size = 564000
			}
			create_pop = {
				culture = royal_harimari
				size = 100000
				religion = the_jadd
			}
			create_pop = {
				culture = sun_elven
				size = 50000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 350000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 450000
			}

		}
	}
	s:STATE_NORTH_GHANKEDHEN = {
		region_state:R02 = {
			create_pop = {
				culture = ghankedhen
				size = 2220000
			}
			create_pop = {
				culture = royal_harimari
				size = 130000
				religion = the_jadd
			}
			create_pop = {
				culture = sun_elven
				size = 30000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 360000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 560000
			}

		}
	}
	s:STATE_TUDHINA = {
		region_state:R03 = {
			create_pop = {
				culture = pasindesh
				size = 1020000
			}
			create_pop = {
				culture = royal_harimari
				size = 70000
			}

		}
	}
	s:STATE_PASIRAGHA = {
		region_state:R01 = {
			create_pop = {
				culture = pasindesh
				size = 9523000
			}
			create_pop = {
				culture = rabhidarubsad
				size = 501000
			}
			create_pop = {
				culture = royal_harimari
				size = 3270000
				split_religion = {
					royal_harimari = {
						the_jadd = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = sun_elven
				size = 70000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 300000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 1190000
			}

		}
	}
	s:STATE_UPPER_DHENBASANA = {
		region_state:R01 = {
			create_pop = {
				culture = rabhidarubsad
				size = 9701000
				split_religion = {
					rabhidarubsad = {
						the_jadd = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = pasindesh
				size = 577000
				split_religion = {
					pasindesh = {
						the_jadd = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = sarniryabsad
				size = 1155000
				split_religion = {
					sarniryabsad = {
						the_jadd = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = surani
				size = 115000
			}
			create_pop = {
				culture = royal_harimari
				size = 8280000
				split_religion = {
					royal_harimari = {
						the_jadd = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = sun_elven
				size = 220000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 1090000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 440000
			}

		}
	}
	s:STATE_LOWER_DHENBASANA = {
		region_state:R01 = {
			create_pop = {
				culture = dhukharuved
				size = 7449000
				split_religion = {
					dhukharuved = {
						the_jadd = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = sarniryabsad
				size = 4966000
				split_religion = {
					sarniryabsad = {
						the_jadd = 0.7
						high_philosophy = 0.3
					}
				}
			}
			create_pop = {
				culture = royal_harimari
				size = 5590000
				split_religion = {
					royal_harimari = {
						the_jadd = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = sun_elven
				size = 20000
			}

		}
	}
	s:STATE_ASCENSION_JUNGLE = {
		region_state:R04 = {
			create_pop = {
				culture = raghamidesh
				size = 520000
				split_religion = {
					raghamidesh = {
						the_jadd = 0.4
						high_philosophy = 0.6
					}
				}
			}
			create_pop = {
				culture = sarniryabsad
				size = 130000
			}
			create_pop = {
				culture = royal_harimari
				size = 1850000
				split_religion = {
					royal_harimari = {
						the_jadd = 0.1
						high_philosophy = 0.9
					}
				}
			}
		}
	}
	s:STATE_TUJGAL = {
		region_state:R04 = {
			create_pop = {
				culture = sarniryabsad
				size = 286000
				split_religion = {
					sarniryabsad = {
						the_jadd = 0.2
						high_philosophy = 0.8
					}
				}
			}
			create_pop = {
				culture = royal_harimari
				size = 1390000
				split_religion = {
					royal_harimari = {
						the_jadd = 0.1
						high_philosophy = 0.9
					}
				}
			}

		}
	}
	s:STATE_BABHAGAMA = {
		region_state:R06 = {
			create_pop = {
				culture = sobhagand
				size = 580000
				split_religion = {
					sobhagand = {
						the_jadd = 0.3
						high_philosophy = 0.7
					}
				}
			}
			create_pop = {
				culture = royal_harimari
				size = 230000
				split_religion = {
					royal_harimari = {
						the_jadd = 0.2
						high_philosophy = 0.8
					}
				}
			}

		}
	}
	s:STATE_SATARSAYA = {
		region_state:R01 = {
			create_pop = {
				culture = dhukharuved
				size = 2402000
				split_religion = {
					dhukharuved = {
						the_jadd = 0.7
						high_philosophy = 0.3
					}
				}
			}
			create_pop = {
				culture = rabhidarubsad
				size = 1030000
				split_religion = {
					rabhidarubsad = {
						the_jadd = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = royal_harimari
				size = 470000
				split_religion = {
					royal_harimari = {
						the_jadd = 0.8
						high_philosophy = 0.2
					}
				}
			}

		}
	}
	s:STATE_WEST_GHAVAANAJ = {
		region_state:R72 = {
			create_pop = {
				culture = ghavaanaj
				size = 2460000
				split_religion = {
					ghavaanaj = {
						godlost = 0.4
						high_philosophy = 0.6
					}
				}
			}
			create_pop = {
				culture = royal_harimari
				size = 30000
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 50000
			}

		}
	}
	s:STATE_EAST_GHAVAANAJ = {
		region_state:R72 = {
			create_pop = {
				culture = ghavaanaj
				size = 2222000
				split_religion = {
					ghavaanaj = {
						godlost = 0.6
						high_philosophy = 0.4
					}
				}
			}
			create_pop = {
				culture = azepyanunin
				size = 1368000
			}
			create_pop = {
				culture = royal_harimari
				size = 90000
				split_religion = {
					royal_harimari = {
						godlost = 0.5
						high_philosophy = 0.5
					}
				}
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 910000
			}

		}
		region_state:R12 = {
			create_pop = {
				culture = dhukharuved
				size = 456000
			}
			create_pop = {
				culture = ghavaanaj
				size = 514000
			}
			create_pop = {
				culture = royal_harimari
				size = 140000
			}
		}
	}
	s:STATE_TUGHAYASA = {
		region_state:R09 = {
			create_pop = {
				culture = azepyanunin
				size = 759000
			}
			create_pop = {
				culture = muthadhaya
				size = 211000
				split_religion = {
					muthadhaya = {
						godlost = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = royal_harimari
				size = 120000
				split_religion = {
					royal_harimari = {
						godlost = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = elephant_hobgoblin
				size = 10000
			}
		}
		region_state:R13 = {
			create_pop = {
				culture = muthadhaya
				size = 1350000
				split_religion = {
					muthadhaya = {
						godlost = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = azepyanunin
				size = 1150000
			}
			create_pop = {
				culture = royal_harimari
				size = 30000
				split_religion = {
					royal_harimari = {
						godlost = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = elephant_hobgoblin
				size = 100000
			}
		}
	}
	s:STATE_DHUJAT = {
		region_state:R10 = {
			create_pop = {
				culture = rasarhid
				size = 8047000
				split_religion = {
					rasarhid = {
						godlost = 0.2
						high_philosophy = 0.9
					}
				}
			}
			create_pop = {
				culture = chengrong
				size = 855000
			}
			create_pop = {
				culture = azepyanunin
				size = 148000
			}
			create_pop = {
				culture = royal_harimari
				size = 3250000
			}
			create_pop = {
				culture = sun_elven
				size = 30000
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 80000
			}

		}
		region_state:R11 = {
			create_pop = {
				culture = rasarhid
				size = 2569000
				split_religion = {
					rasarhid = {
						godlost = 0.3
						high_philosophy = 0.7
					}
				}
			}
			create_pop = {
				culture = chengrong
				size = 394000
			}
			create_pop = {
				culture = azepyanunin
				size = 477000
			}
			create_pop = {
				culture = royal_harimari
				size = 1410000
			}
			create_pop = {
				culture = sun_elven
				size = 20000
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 100000
			}
		}
	}
	s:STATE_SARISUNG = {
		region_state:R13 = {
			create_pop = {
				culture = azepyanunin
				size = 4605000
			}
			create_pop = {
				culture = kintonan
				size = 1337000
			}
			create_pop = {
				culture = rasarhid
				size = 2228000
				split_religion = {
					rasarhid = {
						godlost = 0.7
						high_philosophy = 0.3
					}
				}
			}
			create_pop = {
				culture = sarisungi
				size = 3714000
				split_religion = {
					sarisungi = {
						godlost = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = ghavaanaj
				size = 1040000
				split_religion = {
					ghavaanaj = {
						godlost = 0.6
						high_philosophy = 0.4
					}
				}
			}
			create_pop = {
				culture = szitu
				size = 446000
				split_religion = {
					szitu = {
						godlost = 0.3
						righteous_path = 0.7
					}
				}
			}
			create_pop = {
				culture = muthadhaya
				size = 1486000
				split_religion = {
					muthadhaya = {
						godlost = 0.4
						high_philosophy = 0.6
					}
				}
			}
			create_pop = {
				culture = sun_elven
				size = 40000
				split_religion = {
					sun_elven = {
						godlost = 0.8
						the_jadd = 0.2
					}
				}
			}
			create_pop = {
				culture = elephant_hobgoblin
				size = 2380000
			}

		}
	}
	s:STATE_TILTAGHAR = {
		region_state:R07 = {
			create_pop = {
				culture = rajnadhid
				size = 465000
				split_religion = {
					rajnadhid = {
						godlost = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = azepyanunin
				size = 310000
			}
			create_pop = {
				culture = royal_harimari
				size = 410000
				split_religion = {
					royal_harimari = {
						godlost = 0.7
						high_philosophy = 0.3
					}
				}
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 30000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 60000
			}

		}
	}
	s:STATE_WEST_NADIMRAJ = {
		region_state:R07 = {
			create_pop = {
				culture = shandibad
				size = 756000
				split_religion = {
					shandibad = {
						godlost = 0.6
						high_philosophy = 0.4
					}
				}
			}
			create_pop = {
				culture = azepyanunin
				size = 676000
			}
			create_pop = {
				culture = royal_harimari
				size = 150000
				split_religion = {
					royal_harimari = {
						godlost = 0.6
						high_philosophy = 0.4
					}
				}
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 70000
			}

		}
		region_state:D26 = {
			create_pop = {
				culture = shandibad
				size = 210000
				split_religion = {
					shandibad = {
						godlost = 0.3
						high_philosophy = 0.7
					}
				}
			}
			create_pop = {
				culture = sobhagand
				size = 290000
			}
			create_pop = {
				culture = royal_harimari
				size = 70000
			}
			create_pop = {
				culture = undergrowth_goblin
				size = 1910000
			}
		}
	}
	s:STATE_RAJNADHAGA = {
		region_state:R07 = {
			create_pop = {
				culture = shandibad
				size = 2150000
				split_religion = {
					shandibad = {
						godlost = 0.7
						high_philosophy = 0.3
					}
				}
			}
			create_pop = {
				culture = azepyanunin
				size = 3225000
			}
			create_pop = {
				culture = royal_harimari
				size = 740000
				split_religion = {
					royal_harimari = {
						godlost = 0.6
						high_philosophy = 0.4
					}
				}
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 270000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 340000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 30000
			}

		}
	}
	s:STATE_CENTRAL_NADIMRAJ = {
		region_state:R07 = {
			create_pop = {
				culture = rajnadhid
				size = 770000
				split_religion = {
					rajnadhid = {
						godlost = 1
					}
				}
			}
			create_pop = {
				culture = azepyanunin
				size = 2695000
			}
			create_pop = {
				culture = khedarid
				size = 385000
				split_religion = {
					khedarid = {
						godlost = 1
					}
				}
			}
			create_pop = {
				culture = royal_harimari
				size = 950000
				split_religion = {
					royal_harimari = {
						godlost = 0.7
						high_philosophy = 0.3
					}
				}
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 830000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 300000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 20000
			}

		}
	}
	s:STATE_EAST_NADIMRAJ = {
		region_state:R07 = {
			create_pop = {
				culture = azepyanunin
				size = 1707000
			}
			create_pop = {
				culture = rajnadhid
				size = 1576000
				split_religion = {
					rajnadhid = {
						godlost = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = royal_harimari
				size = 1180000
				split_religion = {
					royal_harimari = {
						godlost = 0.7
						high_philosophy = 0.3
					}
				}
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 410000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 260000
			}

		}
	}
	s:STATE_HOBGOBLIN_HOMELANDS = {
		region_state:R08 = {
			create_pop = {
				culture = azepyanunin
				size = 310000
			}
			create_pop = {
				culture = chimera_hobgoblin
				size = 3400000
			}
			create_pop = {
				culture = march_goblin
				size = 360000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 400000
			}

		}
	}
	s:STATE_GHILAKHAD = {
		region_state:R14 = {
			create_pop = {
				culture = azepyanunin
				size = 656000
			}
			create_pop = {
				culture = khedarid
				size = 353000
				split_religion = {
					khedarid = {
						godlost = 1
					}
				}
			}
			create_pop = {
				culture = royal_harimari
				size = 170000
				split_religion = {
					royal_harimari = {
						godlost = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = chimera_hobgoblin
				size = 90000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 440000
			}

		}
	}
	s:STATE_RAGHAJANDI = {
		region_state:R08 = {
			create_pop = {
				culture = azepyanunin
				size = 4288000
			}
			create_pop = {
				culture = kamtarhid
				size = 476000
			}
			create_pop = {
				culture = royal_harimari
				size = 460000
				split_religion = {
					royal_harimari = {
						godlost = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = chimera_hobgoblin
				size = 4180000
			}
			create_pop = {
				culture = march_goblin
				size = 700000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 1510000
			}

		}
	}
	s:STATE_GHATASAK = {
		region_state:R17 = {
			create_pop = {
				culture = azepyanunin
				size = 622000
			}
			create_pop = {
				culture = kamtarhid
				size = 78000
			}
			create_pop = {
				culture = shamadhan
				size = 78000
			}
			create_pop = {
				culture = royal_harimari
				size = 90000
				split_religion = {
					royal_harimari = {
						godlost = 1
					}
				}
			}
			create_pop = {
				culture = chimera_hobgoblin
				size = 780000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 200000
			}

		}
	}
	s:STATE_SHAMAKHAD_PLAINS = {
		region_state:R15 = {
			create_pop = {
				culture = azepyanunin
				size = 542000
			}
			create_pop = {
				culture = shamadhan
				size = 348000
			}
			create_pop = {
				culture = royal_harimari
				size = 50000
				split_religion = {
					royal_harimari = {
						godlost = 1
					}
				}
			}
			create_pop = {
				culture = chimera_hobgoblin
				size = 130000
			}
			create_pop = {
				culture = march_goblin
				size = 80000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 560000
			}

		}
		region_state:R16 = {
			create_pop = {
				culture = azepyanunin
				size = 842000
			}
			create_pop = {
				culture = shamadhan
				size = 548000
			}
			create_pop = {
				culture = royal_harimari
				size = 230000
				split_religion = {
					royal_harimari = {
						godlost = 1
					}
				}
			}
			create_pop = {
				culture = chimera_hobgoblin
				size = 450000
			}
			create_pop = {
				culture = march_goblin
				size = 180000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 330000
			}

		}
		region_state:R18 = {
			create_pop = {
				culture = azepyanunin
				size = 2802000
			}
			create_pop = {
				culture = shamadhan
				size = 898000
			}
			create_pop = {
				culture = royal_harimari
				size = 440000
				split_religion = {
					royal_harimari = {
						godlost = 1
					}
				}
			}
			create_pop = {
				culture = chimera_hobgoblin
				size = 760000
			}
			create_pop = {
				culture = march_goblin
				size = 260000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 870000
			}
		}
	}
	s:STATE_SIR = {
		region_state:R19 = {
			create_pop = {
				culture = azepyanunin
				size = 5937000
			}
			create_pop = {
				culture = ikaniwagain
				size = 1484000
			}
			create_pop = {
				culture = szitu
				size = 495000
				split_religion = {
					szitu = {
						godlost = 1
					}
				}
			}
			create_pop = {
				culture = shamadhan
				size = 1979000
			}
			create_pop = {
				culture = serene_harimari
				size = 320000
				split_religion = {
					serene_harimari = {
						godlost = 1
					}
				}
			}
			create_pop = {
				culture = dragon_hobgoblin
				size = 3890000
			}
			create_pop = {
				culture = march_goblin
				size = 650000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 1460000
			}

		}
	}
	s:STATE_SRAMAYA = {
		region_state:R05 = {
			create_pop = {
				culture = sarniryabsad
				size = 8701000
			}
			create_pop = {
				culture = dhukharuved
				size = 1783000
			}
			create_pop = {
				culture = chengrong
				size = 594000
			}
			create_pop = {
				culture = royal_harimari
				size = 890000
			}
			create_pop = {
				culture = creek_gnome
				size = 30000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 640000
			}
			create_pop = {
				culture = adasi_lizardfolk
				size = 1770000
			}

		}
		region_state:Y32 = {
			create_pop = {
				culture = sarniryabsad
				size = 810000
			}
			create_pop = {
				culture = royal_harimari
				size = 80000
			}
		}
	}
}