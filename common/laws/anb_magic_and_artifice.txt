﻿law_artifice_banned = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	modifier = {
		country_magical_expertise_decree_cost_mult = -0.75
		country_archmages_pol_str_mult = 0.5
		country_mages_pol_str_mult = 0.5
		country_engineers_pol_str_mult = -0.5
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		hidden_effect = { #accounts for if you had Magic Banned previously
			if = {
				limit = { has_variable = flag:magic_banned_flag }
				remove_variable = flag:magic_banned_flag
				trigger_event = { id = anb_magical_expertise.2 days = 1 }
			}
		}
		if = {
			limit = { has_modifier = eordand_green_artifice_compensation }
			remove_modifier = eordand_green_artifice_compensation
		}
	}

	ai_enact_weight_modifier = {
		value = 0
	}

	progressiveness = -200
	
}

law_nation_of_magic = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	modifier = {
		country_magical_expertise_decree_cost_mult = -0.5
		country_archmages_pol_str_mult = 0.25
		country_mages_pol_str_mult = 0.25
		country_engineers_pol_str_mult = -0.25
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		hidden_effect = { #accounts for if you had Magic Banned previously
			if = {
				limit = { has_variable = flag:magic_banned_flag }
				remove_variable = flag:magic_banned_flag
				trigger_event = { id = anb_magical_expertise.2 days = 1 }
			}
		}
		if = {
			limit = { has_modifier = eordand_green_artifice_compensation }
			remove_modifier = eordand_green_artifice_compensation
		}
	}

	ai_enact_weight_modifier = {
		value = 0
	}

	progressiveness = -100
}

law_nation_of_green_artifice = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	modifier = {
		country_magical_expertise_decree_cost_mult = -0.25
		country_tech_research_speed_mult = 0.025
		country_archmages_pol_str_mult = 0.125
		country_mages_pol_str_mult = 0.125
		country_engineers_pol_str_mult = 0.125
		state_pollution_reduction_health_mult = 1.0
	}
	
	is_visible = {
		has_variable = green_artifice_unlocked
	}
	
	can_enact = {
		has_variable = green_artifice_unlocked
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		hidden_effect = { #accounts for if you had Magic Banned previously
			if = {
				limit = { has_variable = flag:magic_banned_flag }
				remove_variable = flag:magic_banned_flag
				trigger_event = { id = anb_magical_expertise.2 days = 1 }
			}
		}
		if = {
			limit = { 
				OR = { 
					has_law = law_type:law_command_economy
					has_law = law_type:law_laissez_faire
				}
			}
			add_modifier = {	
				name = eordand_green_artifice_compensation
				multiplier = 2
			}
		}
		else_if = {
			limit = { 
				has_law = law_type:law_interventionism
			}
			add_modifier = {	
				name = eordand_green_artifice_compensation
				multiplier = 1
			}
		}
	}

	ai_enact_weight_modifier = {
		value = 0
	}

	progressiveness = 100
}

law_nation_of_artifice = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	disallowing_laws = {
		law_magocracy
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		hidden_effect = { #accounts for if you had Magic Banned previously
			if = {
				limit = { has_variable = flag:magic_banned_flag }
				remove_variable = flag:magic_banned_flag
				trigger_event = { id = anb_magical_expertise.2 days = 1 }
			}
		}
		if = {
			limit = { has_modifier = eordand_green_artifice_compensation }
			remove_modifier = eordand_green_artifice_compensation
		}
	}
	
	modifier = {
		country_tech_research_speed_mult = 0.05
		country_archmages_pol_str_mult = -0.25
		country_mages_pol_str_mult = -0.25
		country_engineers_pol_str_mult = 0.25
	}

	ai_enact_weight_modifier = {
		value = 0
	}

	progressiveness = 100
	
}

law_traditional_magic_banned = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	disallowing_laws = {
		law_magocracy
	}
	
	
	modifier = {
		country_tech_research_speed_mult = 0.1
		country_archmages_pol_str_mult = -0.5
		country_mages_pol_str_mult = -0.5
		country_engineers_pol_str_mult = 0.5
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		hidden_effect = {
			set_variable = flag:magic_banned_flag
			initialize_spell_list = yes 
			remove_variable = military_magic_spell
			remove_variable = military_magic_spell_level
			remove_variable = military_magic_targeted_spell_level
			remove_variable = military_magic_spell_level_floor
			remove_variable = military_magic_spell_changing_level
			update_military_spell = yes
			remove_variable = society_magic_spell
			remove_variable = society_magic_spell_level
			remove_variable = society_magic_targeted_spell_level
			remove_variable = society_magic_spell_level_floor
			remove_variable = society_magic_spell_changing_level
			update_society_spell = yes
			remove_variable = production_magic_spell
			remove_variable = production_magic_spell_level
			remove_variable = production_magic_targeted_spell_level
			remove_variable = production_magic_spell_level_floor
			remove_variable = production_magic_spell_changing_level
			update_production_spell = yes
		}
		if = {
			limit = { has_modifier = eordand_green_artifice_compensation }
			remove_modifier = eordand_green_artifice_compensation
		}
	}

	ai_enact_weight_modifier = {
		value = 0
	}

	progressiveness = 200
}