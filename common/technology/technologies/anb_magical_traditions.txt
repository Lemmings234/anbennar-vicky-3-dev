﻿
cannorian_magical_tradition = {
	era = era_1
	texture = "gfx/interface/icons/invention_icons/invention_placeholder.dds"
	category = society

	can_research = no
	
	ai_weight = {
		value = 0
	}
}

runecarving_magical_tradition = {
	era = era_1
	texture = "gfx/interface/icons/invention_icons/invention_placeholder.dds"
	category = society

	can_research = no
	
	ai_weight = {
		value = 0
	}
}

fourknowing_magical_tradition = {
	era = era_1
	texture = "gfx/interface/icons/invention_icons/invention_placeholder.dds"
	category = society

	can_research = no
	
	ai_weight = {
		value = 0
	}
}

essence_magical_tradition = {
	era = era_1
	texture = "gfx/interface/icons/invention_icons/invention_placeholder.dds"
	category = society

	can_research = no
	
	ai_weight = {
		value = 0
	}
}

chi_cultivation_magical_tradition = {
	era = era_1
	texture = "gfx/interface/icons/invention_icons/invention_placeholder.dds"
	category = society

	can_research = no
	
	ai_weight = {
		value = 0
	}
}

druidic_magical_tradition = {
	era = era_1
	texture = "gfx/interface/icons/invention_icons/invention_placeholder.dds"
	category = society

	can_research = no
	
	ai_weight = {
		value = 0
	}
}

shamanism_magical_tradition = {
	era = era_1
	texture = "gfx/interface/icons/invention_icons/invention_placeholder.dds"
	category = society

	can_research = no
	
	ai_weight = {
		value = 0
	}
}

bardic_magical_tradition = {
	era = era_1
	texture = "gfx/interface/icons/invention_icons/invention_placeholder.dds"
	category = society

	can_research = no
	
	ai_weight = {
		value = 0
	}
}

precursor_magical_tradition = {
	era = era_1
	texture = "gfx/interface/icons/invention_icons/invention_placeholder.dds"
	category = society

	can_research = no
	
	ai_weight = {
		value = 0
	}
}

elemental_magical_tradition = {
	era = era_1
	texture = "gfx/interface/icons/invention_icons/invention_placeholder.dds"
	category = society

	can_research = no
	
	ai_weight = {
		value = 0
	}
}

necromantic_magical_tradition = {
	era = era_1
	texture = "gfx/interface/icons/invention_icons/invention_placeholder.dds"
	category = society

	can_research = no
	
	ai_weight = {
		value = 0
	}
}

kheterata_magical_tradition = {
	era = era_1
	texture = "gfx/interface/icons/invention_icons/invention_placeholder.dds"
	category = society

	can_research = no
	
	ai_weight = {
		value = 0
	}
}