﻿# goods						The good being referenced
# weight					The base weight that is applied to this good based on market Sell Order share
# max_supply_share			The maximum weight that can be applied to this good based on market Sell Order share, relative supply above this amount will have no further impact on base weight
# min_supply_share			If above 0, a minimum of this multiplier of the base weight will be applied to a good regardless of its market Sell Order share

popneed_magical_items = { #TODO: add in buy packages
 	default = magical_reagents

 	entry = {
 		goods = magical_reagents
		
 		weight = 1
 		max_supply_share = 0.4
		min_supply_share = 0
 	} 

 	entry = {
 		goods = curiosity
 		weight = 2
 		max_supply_share = 1
 		min_supply_share = 0
 	} 

	 entry = {
		goods = artificery_doodads
		weight = 1.25
		max_supply_share = 1
		min_supply_share = 0
	} 

}