﻿# Fired off on_monthly_pulse_country

# Coup
coup_monthly_events = {
	random_events = {
		chance_of_no_event = 80
		1 = coup.1
	}
}

# Fund Lobbies Events
fund_lobbies_monthly_events = {  
	random_events = {
		chance_of_no_event = 75
		10 = fund_lobbies_events.1
		10 = fund_lobbies_events.2
		10 = fund_lobbies_events.3
		10 = fund_lobbies_events.4
		10 = fund_lobbies_events.5
	}
}

