﻿anb_on_hold_built = {
	effect = {
		if = {
			limit = { is_building_type = building_dwarven_hold }
			state.state_region = { add_arable_land = 20 }
		}
	}
}

anb_on_hold_expanded = {
	effect = {
		if = {
			limit = { is_building_type = building_dwarven_hold }
			state.state_region = { add_arable_land = 20 }
		}
	}
}

#anbennar natural disaster on action
on_yearly_disaster_events = {
	random_events = {
		chance_of_no_events = 50

		alecand_reclamation.9 = 5
	}
}

on_half_yearly_pulse_state = {	#Anbennar btw
	random_events = { # Adventurers Wanted! 
		#10 = anb_adventurers_wanted.1
		90 = 0
	}
}

on_diplomatic_play_started = {
	effect = {
		
	}
}

anb_on_yearly_events = {
	random_events = {
		chance_of_no_events = 90

		10 = mechanim_revolt.6
	}
}

# Mheromar
anb_mheromar_monthly_events = {
	random_events = {
		chance_of_no_event = 70
		30 = mheromar_events.1
		30 = mheromar_events.2
		2 = mheromar_events.3
		1 = mheromar_events.4
	}
}

anb_mheromar_yearly_events = {
	random_events = {
		chance_of_no_event = 9
		1 = malachite_coup.1
	}
}