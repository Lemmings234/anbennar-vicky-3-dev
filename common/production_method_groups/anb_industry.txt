﻿pmg_method_magical_reagents_workshop = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_patent_stills_magical_reagents
		pm_essence_extraction_magical_reagents_workshop
		pm_synthetic_monster_parts_magical_reagents_workshop
	}
}

pmg_blueblood_magical_reagents_workshop = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_blueblood
		pm_blueblood_extraction
		pm_blueblood_synthesis
	}
}

pmg_automation_magical_reagents_workshop = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_automation_disabled_building_magical_reagents_workshop 
	}
}


pmg_power_supply_building_doodad_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_runic_punch_card_building_doodad_manufacturies
		pm_mana_extraction_treatment_building_doodad_manufacturies
		pm_advanced_mana_extraction_treatment_building_doodad_manufacturies
		pm_thaumaturgical_mana_extraction_treatment_building_doodad_manufacturies
		pm_damestear_core_building_doodad_manufacturies
		pm_damestear_fusion_core_building_doodad_manufacturies
		pm_thaumaturgical_damestear_fusion_core_building_doodad_manufacturies
	}
}

pmg_shell_building_doodad_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_artisan_artificery_building_doodad_manufacturies
		pm_tearite_casings_building_doodad_manufacturies
		pm_mimic_precursor_steel_shell_building_doodad_manufacturies
		pm_modern_precursor_steel_shell_building_doodad_manufacturies
	}
}

pmg_stabilization_building_doodad_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_stabilization_building_doodad_manufacturies
		pm_porcelain_stabilization_building_doodad_manufacturies
		pm_porcelain_integration_building_doodad_manufacturies
		pm_porcelain_rubber_insulation_building_doodad_manufacturies
		pm_maximized_insulation_building_doodad_manufacturies
	}
}

pmg_automation_building_doodad_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_automation_disabled_building_doodad_manufacturies
		pm_rotary_valve_engine_building_doodad_manufacturies
		pm_assembly_lines_building_doodad_manufacturies
		pm_automata_laborers_doodad_manufacturies
		pm_automata_machinists_doodad_manufacturies
	}
}

pmg_control_method_building_automatories = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_natural_production_building_automatories
		pm_control_chip_building_automatories
	}
}

pmg_base_production_building_automatories = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_steelforged_frame
		pm_porcelain_shell
		pm_synthetic_body
		pm_steelforged_frame_freedom
		pm_porcelain_shell_freedom
		pm_synthetic_body_freedom
	}
}

pmg_automation_building_automatories = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_automation
		pm_self_repairing_automata_automatories
		pm_self_building_automata_automatories
	}
}