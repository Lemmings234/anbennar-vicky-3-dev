﻿pmg_base_building_logging_camp = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_simple_forestry
		pm_saw_mills
		pm_electric_saw_mills
	}
}

pmg_hardwood = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_hardwood
		pm_hardwood
		pm_increased_hardwood
	}
}

pmg_anima_building_logging_camp = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_anima_building_logging_camp # Anbennar
		pm_druidic_circle_building_logging_camp # Anbennar
		pm_old_growth_regeneration_building_logging_camp # Anbennar
	}
}

pmg_equipment = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_no_equipment
		pm_steam_donkey_building_logging_camp
		pm_chainsaws 
		pm_shredder_mechs # Anbennar
	}
}

pmg_transportation_building_logging_camp = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_rail_transport_building_logging_camp
		pm_log_carts
	}
}

pmg_base_building_rubber_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		default_building_rubber_plantation
		automatic_irrigation_building_rubber_plantation
	}
}

pmg_enhancements_rubber_plantation = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_growth_bean_rubber_plantation # Anbennar
		high_velocity_irrigation_rubber_plantation # Anbennar
		pm_chronoponics_rubber_plantation # Anbennar
	}
}

pmg_train_automation_building_rubber_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_steam_rail_transport
	}
}

pmg_base_building_fishing_wharf = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_simple_fishing
		pm_fishing_trawlers
		pm_steam_trawlers
	}
}

pmg_enhancements_fishing_wharf = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = { 
		pm_no_enhancements # Anbennar
		pm_lightning_imbued_nets_fishing_wharf # Anbennar
		pm_catch_enlargement_fishing_wharf # Anbennar
	}
}

pmg_refrigeration_building_fishing_wharf = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_unrefrigerated
		pm_refrigerated_rail_cars_building_fishing_wharf
		pm_flash_freezing_building_fishing_wharf
		pm_refrigerated_storage_building_fishing_wharf
	}
}

pmg_base_building_whaling_station = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_simple_whaling
		pm_adventurer_krakeneers # Anbennar
		pm_modern_krakeneer_fleets # Anbennar
	}
}

pmg_enhancements_whaling_station = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = { 
		pm_no_enhancements # Anbennar
		pm_lightning_imbued_harpoons_whaling_station # Anbennar
		pm_catch_enlargement_whaling_station # Anbennar
	}
}

pmg_refrigeration_building_whaling_station = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_unrefrigerated
		pm_refrigerated_storage_building_whaling_station
		pm_refrigerated_rail_cars_building_whaling_station
		pm_flash_freezing_building_whaling_station
	}
}

pmg_base_building_oil_rig = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_steam_derricks
		pm_combustion_derricks
	}
}

pmg_treatments_oil_rig = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_treatments # Anbennar
		pm_damesoil_magnetism # Anbennar
	}
}

pmg_automation_oil_rig = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_automation # Anbennar
		pm_automata_drillers # Anbennar
		pm_automata_drillers_enforced # Anbennar
	}
}

pmg_transportation_building_oil_rig = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_rail_transport_building_oil_rig
		pm_tanker_cars
	}
}
